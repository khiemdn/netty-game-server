package com.vng.server;

import com.google.protobuf.GeneratedMessageV3;
import com.vng.ServerMain;
import com.vng.game.*;
import com.vng.game.params.level.Bonus;
import com.vng.game.params.tree.Reward;
import com.vng.game.params.tree.TreeInfo;
import com.vng.proto.request.RequestCommand;
import com.vng.proto.response.ResponseCommand;
import firebat.db.memcached.BucketManager;
import firebat.log.Log;
import firebat.util.Hash;
import firebat.util.Time;
import io.netty.buffer.*;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.vng.Constant.Constant.*;

public class ServerHandler extends ChannelInboundHandlerAdapter {
    private User channelUser;
    private ScheduledFuture sf;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        if (channelUser != null){
            channelUser.UpdateInfo();
            channelUser.UpdateGame();
            Online.remove(channelUser.getUserId());
            ServerMain.checkDeviceId.remove(channelUser.getDeviceId());
            ServerMain.checkUserId.remove(channelUser.getUserId());
            ServerMain.channelsMap.remove(channelUser.getUserId());
        }
        if (sf != null)
            sf.cancel(true);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf m = (ByteBuf) msg;
        short cmdID = m.readShort();
        int msgType = m.readInt();
        CommandHandler(ctx, m, cmdID, msgType);
    }

    private void CommandHandler(ChannelHandlerContext ctx, ByteBuf m, short cmdID, int msgType) throws Exception {
        switch (cmdID){
            case LOGIN_COMMAND_ID:
                RequestCommand.LoginRequest req = RequestCommand.LoginRequest.parseFrom(new ByteBufInputStream(m));
                String loginHashCheck = Hash.hash("SHA-256", HASH_KEY, req.getUserId(), req.getDeviceId(), req.getOsPlatform(), req.getOsVersion());
                if (!loginHashCheck.equals(req.getHash())){
                    ctx.close();
                    return;
                }
                LoginCmdHandle(req, msgType, ctx);
                break;
            case BUY_COIN_COMMAND_ID:
                RequestCommand.BuyCoin buyCoinReq = RequestCommand.BuyCoin.parseFrom(new ByteBufInputStream(m));
                String buyCoinHashCheck = Hash.hash("SHA-256", HASH_KEY, buyCoinReq.getUserId(), buyCoinReq.getDeviceId(), buyCoinReq.getAmount());
                if (!buyCoinHashCheck.equals(buyCoinReq.getHash())){
                    ctx.close();
                    return;
                }
                BuyCoinCmdHandle(buyCoinReq, ctx);
                break;
            case PLANT_COMMAND_ID:
                RequestCommand.PlantTree plantTreeReq = RequestCommand.PlantTree.parseFrom(new ByteBufInputStream(m));
                String plantTreeRHashCheck = Hash.hash("SHA-256", HASH_KEY, plantTreeReq.getUserId(), plantTreeReq.getDeviceId(), plantTreeReq.getTreeName(), plantTreeReq.getSpentCoin());
                if (!plantTreeRHashCheck.equals(plantTreeReq.getHash())){
                    ctx.close();
                    return;
                }
                PlantTreeCmdHandle(plantTreeReq, ctx);
                break;
            case HARVEST_COMMAND_ID:
                RequestCommand.HarvestTree harvestTreeReq = RequestCommand.HarvestTree.parseFrom(new ByteBufInputStream(m));
                String harvestTreeCheck = Hash.hash("SHA-256", HASH_KEY, harvestTreeReq.getUserId(), harvestTreeReq.getDeviceId(), harvestTreeReq.getTreeName());
                if (!harvestTreeCheck.equals(harvestTreeReq.getHash())){
                    ctx.close();
                    return;
                }
                HarvestCmdHandle(harvestTreeReq, ctx);
                break;
            default:
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Log.exception(cause);
        if (channelUser != null){
            Online.remove(channelUser.getUserId());
            ServerMain.checkDeviceId.remove(channelUser.getDeviceId());
            ServerMain.checkUserId.remove(channelUser.getUserId());
            ServerMain.channelsMap.remove(channelUser.getUserId());
        }
        ctx.close();
    }

    private static ByteBuf getByteBuf (int cmdId, int msgId, GeneratedMessageV3 msg) throws Exception
    {
        int capacity = MAGIC_BYTES_LENGTH + DATA_SIZE_LENGTH + COMMAND_ID_LENGTH + MSG_TYPE_LENGTH + msg.getSerializedSize();
        ByteBuf buf = PooledByteBufAllocator.DEFAULT.ioBuffer(capacity);
        buf.writeByte(MAGIC_BYTE_ACCEPT);
        buf.writeInt(capacity - 5);
        buf.writeShort(cmdId);
        buf.writeInt(msgId);
        msg.writeTo(new ByteBufOutputStream(buf));
        return buf;
    }

    private void LoginCmdHandle(RequestCommand.LoginRequest req, int msgT, ChannelHandlerContext ctx) throws Exception{
        if (req.getUserId() > 0) {
            if (ServerMain.checkUserId.putIfAbsent(req.getUserId(), Boolean.TRUE) != null){
                ctx.close();
                return;
            }
            Info i = Info.get(req.getUserId());
            if (i != null){
                Login(i, req.getDeviceId(), ctx);
                Log.login(i.getUserId(), "_", "_", "_", "_", req.getOsPlatform(), req.getOsVersion(), req.getDeviceId(), "_", "_", i.getLevel(), i.getExp(), channelUser.getCoinBonus() + channelUser.getCoinCash(), channelUser.getCoinCash(), channelUser.getCoinBonus(), "_", 1);
            } else {
                ctx.close();
                return;
            }
        } else {
            if (ServerMain.checkDeviceId.putIfAbsent(req.getDeviceId(), Boolean.TRUE) != null){
                ctx.close();
                return;
            }
            int deviceUserId = DevicesIdMap.get(req.getDeviceId());
            if (deviceUserId == -1) {
                Register(req.getDeviceId(), ctx);
                Log.register(channelUser.getInfo().getUserId(), "_", "_", "_", "_", req.getOsPlatform(), req.getOsVersion(), req.getDeviceId(), "_", "_", channelUser.getInfo().getLevel(), channelUser.getInfo().getExp(), channelUser.getCoinBonus() + channelUser.getCoinCash(), channelUser.getCoinCash(), channelUser.getCoinBonus(), "_", 1);
            } else {
                if (ServerMain.checkUserId.putIfAbsent(deviceUserId, Boolean.TRUE) != null){
                    ctx.close();
                    return;
                }
                Info i = Info.get(deviceUserId);
                if (i != null){
                    Login(i, req.getDeviceId(), ctx);
                    Log.login(i.getUserId(), "_", "_", "_", "_", req.getOsPlatform(), req.getOsVersion(), req.getDeviceId(), "_", "_", i.getLevel(), i.getExp(), channelUser.getCoinBonus() + channelUser.getCoinCash(), channelUser.getCoinCash(), channelUser.getCoinBonus(), "_", 1);
                }
            }
        }
        if (channelUser != null){
            ServerMain.channelsMap.putIfAbsent(channelUser.getUserId(), ctx.channel());
            sf = ctx.channel().eventLoop().scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    ServerMain.udp.write(new InetSocketAddress("localhost", 3031), Unpooled.copiedBuffer("check", CharsetUtil.UTF_8));
                }
            },0, 5, TimeUnit.SECONDS);
            ctx.channel().closeFuture().addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    sf.cancel(true);
                }
            });
        }
    }

    private void BuyCoinCmdHandle(RequestCommand.BuyCoin buyCoinReq, ChannelHandlerContext ctx) throws Exception{
        long oldCc = channelUser.getCoinCash();
        long amount = buyCoinReq.getAmount();
        if (channelUser.buyCoin(buyCoinReq.getAmount()) == -1){
            throw new Exception("Buy coin error");
        }
        Log.paying(channelUser.getUserId(), "_", "_", "_", "_", "_", "_", ctx.channel().remoteAddress().toString(), amount, amount, 0, oldCc, channelUser.getCoinBonus(), 0, channelUser.getCoinCash(), channelUser.getCoinBonus(), "add coin", 1, channelUser.getGame().getLevel(), true, "_" );
        ResponseCommand.BuyCoinResponse res = MakeBuyCoinResponse(true);
        ctx.writeAndFlush(getByteBuf(BUY_COIN_COMMAND_ID, 1, res));
    }

    private void PlantTreeCmdHandle(RequestCommand.PlantTree plantTreeReq, ChannelHandlerContext ctx) throws Exception {
        TreeInfo t = ServerMain.trees.get(plantTreeReq.getTreeName());
        if (t == null){
            ctx.close();
            return;
        }
        boolean status = false;
        int treeUid = 0;
        if (plantTreeReq.getSpentCoin()){
            long oldCoinCash = channelUser.getCoinCash();
            long oldCoinBonus = channelUser.getCoinBonus();
            status = channelUser.spentCoin(t.getPrice().getCoin());
            if (status){
                long cashSpent = oldCoinCash - channelUser.getCoinCash();
                long bonusSpent = oldCoinBonus - channelUser.getCoinBonus();
                treeUid = channelUser.plantTree(t);
                Log.spentCoin("SPEND COINS", channelUser.getUserId(), "_", "_", "_", channelUser.getGame().getLevel(), "_", ctx.channel().remoteAddress().toString(), true, plantTreeReq.getTreeName(), plantTreeReq.getTreeName(), t.getPrice().getCoin(), 1, t.getPrice().getCoin(), cashSpent, bonusSpent, channelUser.getCoinCash() + channelUser.getCoinBonus(), channelUser.getCoinCash(), channelUser.getCoinBonus(), "plant tree", 1, "_");
                Log.ifrs(channelUser.getUserId(), oldCoinCash, oldCoinBonus, Time.currentTimeMillis(), "_", cashSpent, bonusSpent, plantTreeReq.getTreeName(), PLANT_COMMAND_ID, t.getPrice().getCoin(), cashSpent, cashSpent, true, "_");
            }
        } else {
            status = channelUser.spentGold(t.getPrice().getGold());
        }
        Log.action("Plant tree", channelUser.getUserId(), "_", "_", "_", channelUser.getGame().getLevel(), Time.currentTimeMillis() - channelUser.getGame().getTimeLogin(), 1, "_", "_", "_");
        ResponseCommand.PlantTreeResponse res = MakePlantTreeResponse(status, t, plantTreeReq.getTreeName(), treeUid);
        ctx.writeAndFlush(getByteBuf(PLANT_COMMAND_ID, 1, res));
    }

    private void HarvestCmdHandle(RequestCommand.HarvestTree harvestTreeReq, ChannelHandlerContext ctx) throws Exception{
        TreeInfo t = ServerMain.trees.get(harvestTreeReq.getTreeName());
        int treeUid = harvestTreeReq.getTreeUid();
        if (channelUser.harvestTree(treeUid)) {
            int oldLevel = channelUser.getGame().getLevel();
            ConcurrentHashMap<Integer, Bonus> bonuses = channelUser.addExp(t.getReward().getExp());
            Log.action("Harvest tree", channelUser.getUserId(), "_", "_", "_", channelUser.getGame().getLevel(), Time.currentTimeMillis() - channelUser.getGame().getTimeLogin(), 1, "_", "_", "_");
            if (oldLevel < channelUser.getGame().getLevel())
                Log.levelUp(channelUser.getUserId(), "_", "_", "_", oldLevel, channelUser.getGame().getLevel(), channelUser.getGame().getExp(), Time.currentTimeMillis() - channelUser.getGame().getTimeLogin(), "levelup after harvest");
            ResponseCommand.HarvestResponse res = MakeHarvestResponse(true, harvestTreeReq.getTreeName(), t.getReward(), null);
            ctx.writeAndFlush(getByteBuf(HARVEST_COMMAND_ID, 1, res));
        }
    }

    private void Login(Info i, String deviceId, ChannelHandlerContext ctx) throws Exception{
        if (!Online.add(i.getUserId(), ctx.channel().remoteAddress().toString())){
            throw new Exception("Online key exists when login");
        }
        channelUser = User.get(i, deviceId);
        if (channelUser != null && channelUser.getGame().getDevicesId().contains(deviceId)){
            channelUser.getGame().setTimeLogin(Time.currentTimeMillis());
            DevicesIdMap.reMap(deviceId, i.getUserId());
            ResponseCommand.LoginResponse res = MakeLoginResponse(channelUser);
            ctx.writeAndFlush(getByteBuf(LOGIN_COMMAND_ID, DEFAULT_MESSAGE_TYPE, res));
            return;
        }
        Online.remove(i.getUserId());
        throw new Exception("User get null when login");
    }

    private void Register(String deviceId, ChannelHandlerContext ctx) throws Exception{
        String bucketId = BucketManager.getSmallestUserBucket();
        channelUser = User.create(bucketId, deviceId);
        if (channelUser != null){
            UserCount.increase();
            ServerMain.checkUserId.putIfAbsent(channelUser.getUserId(), Boolean.TRUE);
            DevicesIdMap.add(deviceId, channelUser.getUserId());
            if (!Online.add(channelUser.getUserId(), ctx.channel().remoteAddress().toString())){
                throw new Exception("Online key exists when register");
            }
            ResponseCommand.LoginResponse res = MakeLoginResponse(channelUser);
            ctx.writeAndFlush(getByteBuf(LOGIN_COMMAND_ID, DEFAULT_MESSAGE_TYPE, res));
            return;
        }
        throw new Exception("User create null when register");
    }

    private ResponseCommand.LoginResponse MakeLoginResponse(User u) throws Exception{
        String key = Hash.hash("SHA-256", HASH_KEY_CHAT, channelUser.getUserId(), channelUser.getDeviceId(), channelUser.getGame().getTimeLogin());
        Game game = u.getGame();
        Chat.add(u.getUserId(), key);
        ResponseCommand.LoginResponse res = ResponseCommand.LoginResponse.newBuilder()
                .setGame(ResponseCommand.Game.newBuilder()
                        .setLevel(game.getLevel())
                        .setExp(game.getExp())
                        .setGold(game.getGold())
                        .setTimeLogin(game.getTimeLogin())
                        .setTimeRegister(game.getTimeRegister())
                        .build())
                .setUserId(u.getUserId())
                .setCoinBonus(u.getCoinBonus())
                .setCoinCash(u.getCoinCash())
                .setTime(channelUser.getGame().getTimeLogin())
                .setKeyChat(key)
                .build();
        return res;
    }

    private ResponseCommand.BuyCoinResponse MakeBuyCoinResponse(boolean status){
        Game g = channelUser.getGame();
        return ResponseCommand.BuyCoinResponse.newBuilder()
                .setGame(ResponseCommand.Game.newBuilder()
                        .setLevel(g.getLevel())
                        .setExp(g.getExp())
                        .setGold(g.getGold())
                        .setTimeLogin(g.getTimeLogin())
                        .setTimeRegister(g.getTimeRegister())
                        .build())
                .setCoinCash(channelUser.getCoinCash())
                .setCoinBonus(channelUser.getCoinBonus())
                .setSuccess(status)
                .setTime(Time.currentTimeMillis())
                .build();
    }

    private ResponseCommand.PlantTreeResponse MakePlantTreeResponse(boolean status, TreeInfo t, String treeName, int treeUid){
        Game g = channelUser.getGame();
        return ResponseCommand.PlantTreeResponse.newBuilder()
                .setGame(ResponseCommand.Game.newBuilder()
                        .setLevel(g.getLevel())
                        .setExp(g.getExp())
                        .setGold(g.getGold())
                        .setTimeLogin(g.getTimeLogin())
                        .setTimeRegister(g.getTimeRegister())
                        .build())
                .setCoinCash(channelUser.getCoinCash())
                .setCoinBonus(channelUser.getCoinBonus())
                .setTreeName(treeName)
                .setTimeGrow(t.getTimeGrow())
                .setSuccess(status)
                .setTime(Time.currentTimeMillis())
                .setTreeUid(treeUid)
                .build();
    }

    private ResponseCommand.HarvestResponse MakeHarvestResponse(boolean status, String treeName, Reward reward, ConcurrentHashMap<Integer, Bonus> bonuses){
        Game g = channelUser.getGame();
        ResponseCommand.HarvestResponse.Builder b = ResponseCommand.HarvestResponse.newBuilder()
                .setGame(ResponseCommand.Game.newBuilder()
                        .setLevel(g.getLevel())
                        .setExp(g.getExp())
                        .setGold(g.getGold())
                        .setTimeLogin(g.getTimeLogin())
                        .setTimeRegister(g.getTimeRegister())
                        .build())
                .setCoinCash(channelUser.getCoinCash())
                .setCoinBonus(channelUser.getCoinBonus())
                .setTreeName(treeName)
                .setSuccess(status)
                .setTime(Time.currentTimeMillis());
        if (!status) return b.build();
        if (reward != null){
            b.setReward(ResponseCommand.Reward.newBuilder().setExp(reward.getExp()).build());
        }
        if (bonuses != null){
            for (Map.Entry<Integer, Bonus> bonus : bonuses.entrySet()) {
                b.addBonus(ResponseCommand.Bonus.newBuilder().setCoin(bonus.getValue().getCoin()).setGold(bonus.getValue().getGold()).build());
            }
        }
        return b.build();
    }
}
