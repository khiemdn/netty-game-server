package com.vng.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import static com.vng.Constant.Constant.MAGIC_BYTE_ACCEPT;

public class ServerLengthEncoder extends MessageToByteEncoder<ByteBuf> {

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) throws Exception {
        ByteBuf encoded = ctx.alloc().ioBuffer();
        encoded.writeByte(MAGIC_BYTE_ACCEPT);
        encoded.writeInt(msg.readableBytes());
        encoded.writeBytes(msg);
        out.writeBytes(encoded);
    }
}
