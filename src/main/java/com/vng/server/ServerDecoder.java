package com.vng.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import static com.vng.Constant.Constant.*;

public class ServerDecoder extends ByteToMessageDecoder {
    private boolean isClosed = false;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (isClosed){
            skipBytes(in);
            return;
        }
        in.markReaderIndex();
        byte magic = in.readByte();
        if (magic != MAGIC_BYTE_ACCEPT){
            close(ctx, in);
            return;
        }
        if (in.readableBytes() < DATA_SIZE_LENGTH){
            return;
        }
        int dataLength = in.readInt();
        if (dataLength <= 0 || dataLength >= MAX_CLIENT_PACKAGE_SIZE){
            close(ctx, in);
            return;
        }
        if (dataLength > in.readableBytes()){
            in.resetReaderIndex();
        } else {
            out.add(in.readBytes(dataLength));
        }
    }

    private void skipBytes (ByteBuf in) {
        int len = in.readableBytes();
        if (len > 0)
            in.skipBytes(len);
    }

    private void close (ChannelHandlerContext ctx, ByteBuf in) {
        isClosed = true;
        skipBytes(in);
        ctx.close();
    }
}
