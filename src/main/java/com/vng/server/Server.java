package com.vng.server;

import firebat.io.socket.SocketServer;

public class Server {
    private static SocketServer socketServer;
    public Server(String id) {
        socketServer = new SocketServer("game server");
    }

    public void run(String host, int port) throws Exception{
        socketServer.start(host, port, new ServerInitializer());
    }

    public void stop(){
        socketServer.stop();
    }
}
