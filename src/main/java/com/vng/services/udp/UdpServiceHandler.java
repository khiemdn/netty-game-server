package com.vng.services.udp;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;


public class UdpServiceHandler extends SimpleChannelInboundHandler<DatagramPacket> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket data) throws Exception {
        System.out.println(data.content().toString(CharsetUtil.UTF_8));
        ctx.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer("msg", CharsetUtil.UTF_8), data.sender()));
    }
}
