package com.vng.services.udp;

import firebat.io.udp.Udp;

public class UdpService {
    private static Udp udp;
    public static void main(String args[]){
        try {
            udp = new Udp("1");
            udp.start("localhost", 3031, new UdpServiceHandler());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
