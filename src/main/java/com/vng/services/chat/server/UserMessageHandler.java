package com.vng.services.chat.server;

import com.vng.game.Chat;
import com.vng.proto.request.RequestCommand;
import firebat.log.*;
import firebat.util.Hash;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.channel.group.ChannelGroupFutureListener;
import io.netty.channel.group.ChannelMatchers;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.vng.Constant.Constant.HASH_KEY_CHAT;

public class UserMessageHandler extends ChannelInboundHandlerAdapter {
    private int userID;
    private int roomID;
    private Channel channel;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf m = (ByteBuf) msg;
        byte magic = m.readByte();
        if (magic == 0) {
            RequestCommand.JoinChatRoom req = RequestCommand.JoinChatRoom.parseFrom(new ByteBufInputStream(m));

            String keyCheck = Hash.hash("SHA-256", HASH_KEY_CHAT, req.getUserId(), req.getDeviceId(), req.getTimeLogin());
            if (!req.getKeyChat().equals(keyCheck)) {
                ctx.close();
                return;
            }
            String dbkey = Chat.get(req.getUserId());
            if (!req.getKeyChat().equals(dbkey)) {
                ctx.close();
                return;
            }
            this.channel = ctx.channel();
            this.userID = req.getUserId();
            this.roomID = ChatServer.roomIdGenerator.getAndIncrement();

            int roomID = ChatServer.AutoJoinRoom(ctx.channel());
            ChatServer.userMap.putIfAbsent(userID, this);

            ctx.writeAndFlush(Unpooled.copiedBuffer(String.valueOf(roomID), CharsetUtil.UTF_8));
            return;
        } else {
            ChatRoom(m.toString(CharsetUtil.UTF_8), ctx);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent){
            switch (((IdleStateEvent) evt).state()){
                case ALL_IDLE:
                    ctx.close().sync();
                    break;
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }

    private void ChatRoom(String msg, ChannelHandlerContext ctx){
        ChannelGroup group = ChatServer.roomMap.get(roomID);
        String msgSend = String.format("user%d: %s\r\n", userID, msg);
        ByteBuf dataSend = ctx.alloc().ioBuffer();
        dataSend.writeCharSequence(msgSend, CharsetUtil.UTF_8);
        group.writeAndFlush(dataSend, ChannelMatchers.isNot(ctx.channel())).addListener(new ChannelGroupFutureListener() {
            @Override
            public void operationComplete(ChannelGroupFuture future) throws Exception {
                Log.debug(String.valueOf(roomID), userID,  msg);
            }
        });
    }
}
