package com.vng.services.chat.server;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ChatServer {
    private int port;
    private ServerBootstrap serverBootstrap;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    public static AtomicInteger roomIdGenerator;
    public static ConcurrentHashMap<Integer, ChannelGroup> roomMap;
    public static ConcurrentHashMap<Integer, UserMessageHandler> userMap;
    public static final int MAX = 2;
    public ChatServer(int port) {
        this.port = port;
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        serverBootstrap = new ServerBootstrap();
        roomMap = new ConcurrentHashMap<>();
        userMap = new ConcurrentHashMap<>();
        roomIdGenerator = new AtomicInteger();
        serverBootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                .addLast(new IdleStateHandler(0, 0, 180, TimeUnit.SECONDS))
                                .addLast(new MessageDecoder())
                                .addLast(new UserMessageHandler());
                    }
                })
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.TCP_NODELAY, true);
    }

    public void run() throws Exception {
        try {
            ChannelFuture c = serverBootstrap.bind(port).sync();

            c.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    private static ChannelGroup CreateGroup(){
        return new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    }

    public static synchronized int AutoJoinRoom(Channel ch){
        for (Map.Entry<Integer, ChannelGroup> groupEntry : roomMap.entrySet()) {
            if (groupEntry.getValue().size() < MAX){
                groupEntry.getValue().add(ch);
                return groupEntry.getKey();
            }
        }
        ChannelGroup channelGroup = CreateGroup();
        int id = roomIdGenerator.getAndIncrement();
        roomMap.putIfAbsent(id, channelGroup);
        channelGroup.add(ch);
        return id;
    }

    public void Stop(){
        for (Map.Entry<Integer, ChannelGroup> room : roomMap.entrySet()) {
            ChannelGroup group = room.getValue();
            group.writeAndFlush(Unpooled.copiedBuffer("Server is closing\r\n", CharsetUtil.UTF_8)).addListener(ChannelFutureListener.CLOSE);
        }
        roomMap.clear();
        userMap.clear();
    }
}
