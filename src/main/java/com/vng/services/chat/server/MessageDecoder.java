package com.vng.services.chat.server;

import com.vng.game.Chat;
import com.vng.proto.request.RequestCommand;
import firebat.util.Hash;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.CharsetUtil;

import java.util.List;

import static com.vng.Constant.Constant.HASH_KEY_CHAT;
import static com.vng.Constant.Constant.MAX_CLIENT_PACKAGE_SIZE;

public class MessageDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() <= 4) //4 byte len, x byte data
            return;
        in.markReaderIndex();
        int bodyLen = in.readInt();

        if (bodyLen <= 0 || bodyLen > MAX_CLIENT_PACKAGE_SIZE)
        {
            ctx.close();
            return;
        }
        else if (bodyLen > in.readableBytes())
        {
            in.resetReaderIndex();
        }
        else
        {
            out.add(in.readBytes(bodyLen));
        }
    }
}
