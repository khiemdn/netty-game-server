package com.vng.services.chat;

import firebat.db.memcached.BucketManager;
import firebat.log.*;
import com.vng.services.chat.server.ChatServer;

public class ChatMain {
    public static void main(String args[]){
        try {
            Log.start("./config/log.properties", true);
            BucketManager.start("./config/memcached.json");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ChatServer server = new ChatServer(8080);
        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new Thread(() -> {
            server.Stop();
        }));

        try {
            server.run();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
