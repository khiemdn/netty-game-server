package com.vng;

import com.google.gson.reflect.TypeToken;
import com.vng.game.User;
import com.vng.game.UserCount;
import com.vng.game.params.level.LevelInfo;
import com.vng.game.params.tree.TreeInfo;
import com.vng.server.ServiceHandler;
import firebat.db.memcached.BucketManager;
import firebat.io.udp.Udp;
import firebat.log.Log;
import firebat.util.AtomicInteger;
import firebat.util.Environment;
import firebat.util.Json;
import firebat.util.ServiceStatus;
import io.netty.channel.Channel;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class ServerMain {
    private final static ServiceStatus status = new ServiceStatus(true);
    private static Environment environment;
    private static com.vng.server.Server server;
    public static ConcurrentHashMap<Integer, Channel> channelsMap;
    public static ConcurrentHashMap<Integer, Boolean> checkUserId;
    public static ConcurrentHashMap<String, Boolean> checkDeviceId;
    public static ArrayList<LevelInfo> levelInfos;
    public static ConcurrentHashMap<String, TreeInfo> trees;
    public static int maxLevel;

    public static Udp udp;

    public static void main(String args[]){
        File storageDir = new File("../_database/cache");
        if (storageDir.exists()) {
            for (File tempFile : storageDir.listFiles()) {
                tempFile.delete();
            }
        }
        try {
            status.setStartingUp();
            addShutdownHook();
            if (args != null && args.length > 0) {
                environment = Environment.valueOf(args[0]);
            } else {
                environment = Environment.DEV;
            }

            Log.start("./config/log.properties", true);
            BucketManager.start("./config/memcached.json");
            int userCount = UserCount.Load();
            if (userCount < 0){
                throw new Exception("Cant get user count");
            }
            channelsMap = new ConcurrentHashMap<>();
            checkDeviceId = new ConcurrentHashMap<>();
            checkUserId = new ConcurrentHashMap<>();
            User.idGen = new AtomicInteger(userCount + 1);

            LoadConstant();

            server = new com.vng.server.Server("game server");
            server.run("localhost", 8030);
            udp = new Udp("1");
            udp.start("localhost", 3030, new ServiceHandler());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void LoadConstant() throws IOException {
        String levelsConfig = new String(Files.readAllBytes(Paths.get("./config/levels.json")));
        String treesConfig = new String(Files.readAllBytes(Paths.get("./config/trees.json")));
        Type levelsType = new TypeToken<ArrayList<LevelInfo>>(){}.getType();
        Type treesType = new TypeToken<ConcurrentHashMap<String, TreeInfo>>(){}.getType();
        levelInfos = Json.gson.fromJson(levelsConfig, levelsType);
        levelInfos.sort((o1, o2) -> o1.getLevel() - o2.getLevel());
        trees = Json.gson.fromJson(treesConfig, treesType);
        maxLevel = levelInfos.get(levelInfos.size() - 1).getLevel();
    }

    private static synchronized void stop(){
        File storageDir = new File("../_database/cache");
        for(File tempFile : storageDir.listFiles()) {
            tempFile.delete();
        }
        status.setShuttingDown();
        if (server != null){
            server.stop();
        }
        Log.stop();
        status.setShutdown();
    }

    private static void addShutdownHook(){
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                try {
                    ServerMain.stop();
                }catch (Exception e){
                    Log.exception(e);
                }
            }
        });
    }

    public static boolean isRunning () {
        return status.isStarted();
    }
}
