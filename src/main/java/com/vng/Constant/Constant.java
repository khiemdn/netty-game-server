package com.vng.Constant;

public final class Constant {
    // data format
    public static final int MAGIC_BYTES_LENGTH = 1;
    public static final int DATA_SIZE_LENGTH = 4;
    public static final int COMMAND_ID_LENGTH = 2;
    public static final int MSG_TYPE_LENGTH = 4;
    public static final int MAX_CLIENT_PACKAGE_SIZE = 65000;

    // magic byte
    public static final int MAGIC_BYTE_ACCEPT = 9;

    // message types
    public static final int DEFAULT_MESSAGE_TYPE = 1;

    // hash key
    public static final String HASH_KEY = "xArde35;&wU#R2NR";
    public static final String HASH_KEY_CHAT = "d23as12;&w21aewR";

    // Command id
    public static final short LOGIN_COMMAND_ID = 1;
    public static final short PLANT_COMMAND_ID = 2;
    public static final short HARVEST_COMMAND_ID = 3;
    public static final short BUY_COIN_COMMAND_ID = 4;
    public static final short BUY_GOLD_COMMAND_ID = 5;
}
