package com.vng.game;

import com.vng.ServerMain;
import com.vng.game.params.level.Bonus;
import com.vng.game.params.level.LevelInfo;
import com.vng.game.params.tree.TreeInfo;
import firebat.log.Log;
import firebat.util.AtomicInteger;

import java.util.concurrent.ConcurrentHashMap;

public class User {
    public static AtomicInteger idGen;
    private Game game;
    private Info info;
    private long coinCash, coinBonus;
    private int userId;
    private String bucketId;
    private String deviceId;

    private User(Game game, Info info, long coinCash, long coinBonus, int userId, String bucketId, String deviceId) {
        this.game = game;
        this.info = info;
        this.coinCash = coinCash;
        this.coinBonus = coinBonus;
        this.userId = userId;
        this.bucketId = bucketId;
        this.deviceId = deviceId;
    }

    public synchronized static User create(String bucketId, String deviceId){
        int newId = idGen.getAndIncrement();
        Info i = Info.add(bucketId, newId);
        Game g = Game.add(bucketId, newId, deviceId);
        long cc = CoinCash.add(bucketId, newId);
        long cb = CoinBonus.add(bucketId, newId);
        if (i == null || g == null || cc < 0 || cb < 0){
            return null;
        }
        return new User(g, i, cc, cb, newId, bucketId, deviceId);
    }

    public static User get(Info i, String deviceId){
        Game g = Game.get(i.getBucketId(), i.getUserId(), deviceId);
        if (g == null) {
            return null;
        }
        long cc = CoinCash.get(i.getBucketId(), i.getUserId());
        long cb = CoinBonus.get(i.getBucketId(), i.getUserId());
        if (g == null || cc < 0 || cb < 0)
            return null;
        return new User(g, i, cc, cb, i.getUserId(), i.getBucketId(), deviceId);
    }

    public long buyCoin(int amount){
        coinCash = CoinCash.increase(bucketId, userId, amount);
        return coinCash;
    }

    public ConcurrentHashMap<Integer, Bonus> addExp(long amount){
        long exp = game.addExp(amount);
        int currentLevel = game.getLevel();
        int nextLevelIndex = 0;
        ConcurrentHashMap<Integer, Bonus> levelBonuses = new ConcurrentHashMap<>();
        if (currentLevel == ServerMain.maxLevel){
            return levelBonuses;
        }
        if (currentLevel != 0){
            for (int i = 0; i < ServerMain.levelInfos.size(); i++){
                if (ServerMain.levelInfos.get(i).getLevel() == currentLevel){
                    nextLevelIndex = i+1;
                    break;
                }
            }
        }
        LevelInfo levelInfo = ServerMain.levelInfos.get(nextLevelIndex);
        long requireExp = levelInfo.getExp();
        if (exp < requireExp){
            return levelBonuses;
        }
        long goldBonus = 0;
        long coinBonus = 0;
        int finalLevel = currentLevel;
        while (exp >= requireExp && requireExp > 0) {
            finalLevel = levelInfo.getLevel();
            Log.debug(nextLevelIndex);
            Bonus bonus = levelInfo.getBonus();
            if (bonus == null) {
                bonus = new Bonus();
            }
            levelBonuses.put(nextLevelIndex, bonus);
            nextLevelIndex++;
            exp -= requireExp;
            goldBonus+= bonus.getGold();
            coinBonus+= bonus.getCoin();
            if (nextLevelIndex >= ServerMain.levelInfos.size())
                break;
            levelInfo = ServerMain.levelInfos.get(nextLevelIndex);
            requireExp = levelInfo.getExp();
        }
        game.setExp(exp);
        game.addLevel(finalLevel - currentLevel);
        game.increaseGold(goldBonus);
        CoinBonus.increase(bucketId, userId, coinBonus);
        game.update(bucketId, userId);
        return levelBonuses;
    }

    public boolean spentCoin(int amount){
        if (amount > coinCash + coinBonus){
            return false;
        }
        long extra = 0;
        if (amount > coinCash){
            extra = amount - coinCash;
            coinCash = CoinCash.decrease(bucketId, userId, coinCash);
            coinBonus = CoinBonus.decrease(bucketId, userId, extra);
        } else {
            coinCash = CoinCash.decrease(bucketId, userId, amount);
        }
        if (coinBonus >= 0 && coinCash >= 0) {
            return true;
        }
        return false;
    }

    public int plantTree(TreeInfo treeInfo){
        int uid = game.addTree(treeInfo);
        game.update(bucketId, userId);
        return uid;
    }

    public boolean harvestTree(int uid){
        if (game.removeTree(uid)){
            game.update(bucketId, userId);
            return true;
        }
        return false;
    }

    public boolean spentGold(long amount) {
        if (amount > game.getGold()){
            return false;
        }
        game.decreaseGold(amount);
        game.update(bucketId, userId);
        return true;
    }

    public void UpdateInfo(){
        info.update();
    }

    public void UpdateGame(){
        game.update(bucketId, userId);
    }

    public Game getGame() {
        return game;
    }

    public long getCoinCash() {
        return coinCash;
    }

    public long getCoinBonus() {
        return coinBonus;
    }

    public int getUserId() {
        return userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Info getInfo() {
        return info;
    }
}
