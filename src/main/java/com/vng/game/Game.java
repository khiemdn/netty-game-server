package com.vng.game;

import com.vng.game.params.tree.TreeInfo;
import firebat.db.memcached.BucketManager;
import firebat.util.Json;
import firebat.util.Time;

import java.util.ArrayList;
import java.util.HashSet;

public class Game {
    private static final String DB_SUFFIX = "_game";
    private int uidCount;
    private int level;
    private long exp;
    private long gold;
    private long timeRegister;
    private long timeLogin;
    private HashSet<String> devicesId;
    private ArrayList<Tree> trees;

    private Game(int uidCount, int level, int exp, long gold, long timeRegister, long timeLogin, HashSet<String> devicesId, ArrayList<Tree> trees) {
        this.level = level;
        this.exp = exp;
        this.gold = gold;
        this.uidCount = uidCount;
        this.timeRegister = timeRegister;
        this.timeRegister = timeRegister;
        this.timeLogin = timeLogin;
        this.devicesId = devicesId;
        this.trees = trees;
    }

    public static Game add(String bucketId, int userId, String deviceId){
        long currentTime = Time.currentTimeMillis();
        HashSet<String> devices = new HashSet<>();
        devices.add(deviceId);
        Game g = new Game(0, 0, 0, 0, currentTime, currentTime, devices, new ArrayList<>());
        String key = userId + DB_SUFFIX;
        if (BucketManager.get(bucketId).add(key, g.toJson()))
            return g;
        else
            return null;
    }
    public static Game get(String bucketId, int userId, String deviceId){
        String key = userId + DB_SUFFIX;
        String result = (String) BucketManager.get(bucketId).get(key);
        Game g = Json.fromJson(result, Game.class);
        if (g == null){
            return g;
        }
        g.timeLogin = Time.currentTimeMillis();
        g.devicesId.add(deviceId);
        return g;
    }

    public int addTree(TreeInfo treeInfo){
        int uid = nextUid();
        Tree t = new Tree(uid, treeInfo.getTimeGrow(), treeInfo.getItemId());
        trees.add(t);
        return uid;
    }

    private int nextUid(){
        return ++uidCount;
    }

    public boolean removeTree(int uid){
        return trees.removeIf(tree -> tree.getUid() == uid);
    }

    public long addExp(long amount) {
        exp += amount;
        return exp;
    }

    public void setExp(long amount) {
        exp = amount;
    }

    public void addLevel(int amount) {
        level += amount;
    }


    private String toJson(){
        return Json.toJsonPretty(this);
    }

    public boolean update(String bucketId, int userId){
        String key = userId + DB_SUFFIX;
        return BucketManager.get(bucketId).set(key, toJson());
    }

    public void increaseGold(long amount) {
        gold += amount;
    }

    public void decreaseGold(long amount) {
        gold -= amount;
    }

    public int getLevel() {
        return level;
    }

    public long getExp() {
        return exp;
    }

    public long getTimeRegister() {
        return timeRegister;
    }

    public long getTimeLogin() {
        return timeLogin;
    }

    public HashSet<String> getDevicesId() {
        return devicesId;
    }

    public long getGold() {
        return gold;
    }

    public void setTimeLogin(long timeLogin) {
        this.timeLogin = timeLogin;
    }
}
