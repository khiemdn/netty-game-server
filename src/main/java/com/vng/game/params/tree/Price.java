package com.vng.game.params.tree;

public class Price {
    private int coin;
    private long gold;

    public int getCoin() {
        return coin;
    }

    public long getGold() {
        return gold;
    }
}
