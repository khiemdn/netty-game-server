package com.vng.game.params.tree;

public class TreeInfo {
    private int timeGrow;
    private Reward reward;
    private Price price;
    private int itemId;

    public int getTimeGrow() {
        return timeGrow;
    }

    public Reward getReward() {
        return reward;
    }

    public Price getPrice() {
        return price;
    }

    public int getItemId() {
        return itemId;
    }
}
