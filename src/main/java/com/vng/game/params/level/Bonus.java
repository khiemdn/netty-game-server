package com.vng.game.params.level;

public class Bonus {
    private long coin;
    private long gold;

    public long getCoin() {
        return coin;
    }

    public long getGold() {
        return gold;
    }
}
