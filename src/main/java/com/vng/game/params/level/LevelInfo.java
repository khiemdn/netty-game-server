package com.vng.game.params.level;

public class LevelInfo {
    private int level;
    private long exp;
    private Bonus bonus;

    public int getLevel() {
        return level;
    }

    public long getExp() {
        return exp;
    }

    public Bonus getBonus() {
        return bonus;
    }

}
