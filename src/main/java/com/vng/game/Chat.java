package com.vng.game;

import com.google.gson.Gson;
import firebat.db.memcached.BucketManager;

public class Chat {
    private static final String BUCKET_ID = "cache";
    private static final String DB_KEY_SUFFIX = "_chat";

    public static boolean add(int userId, String chatKey){
        String key = userId + DB_KEY_SUFFIX;
        return BucketManager.get(BUCKET_ID).add(key, chatKey);
    }

    public static String get(int userId){
        String key = userId + DB_KEY_SUFFIX;
        return (String) BucketManager.get(BUCKET_ID).get(key);
    }

    public static boolean remove(int userId){
        String key = userId + DB_KEY_SUFFIX;
        return BucketManager.get(BUCKET_ID).delete(key);
    }
}
