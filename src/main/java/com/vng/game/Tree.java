package com.vng.game;

public class Tree {
    private int uid;
    private long timePlant;
    private int itemId;

    public Tree(int uid, long timePlant, int itemId) {
        this.uid = uid;
        this.timePlant = timePlant;
        this.itemId = itemId;
    }

    public int getUid() {
        return uid;
    }

    public long getTimePlant() {
        return timePlant;
    }

    public int getItemId() {
        return itemId;
    }
}
