package com.vng.game;

import firebat.db.memcached.BucketManager;
import firebat.util.Json;

public class Info {
    private static final String BUCKET_ID = "index";
    private static final String DB_SUFFIX = "_info";

    private String bucketId;
    private transient int userId;
    private int level;
    private int exp;

    public Info(String bucketId, int userId, int level, int exp) {
        this.bucketId = bucketId;
        this.userId = userId;
        this.level = level;
        this.exp = exp;
    }

    public static Info get(int userId){
        String key = userId + DB_SUFFIX;
        Object value = BucketManager.get(BUCKET_ID).get(key);
        if (value == null) {
            return null;
        }
        String jsonStr;
        if (value instanceof String)
            jsonStr = (String)value;
        else
            jsonStr = new String((byte[]) (value));
        Info i = Json.fromJson(jsonStr, Info.class);
        i.userId = userId;
        return i;
    }

    public static Info add(String bucketId, int userId){
        Info i = new Info(bucketId, userId, 0, 0);
        String key = userId + DB_SUFFIX;
        if (BucketManager.get(BUCKET_ID).add(key, i.toJSON())){
            return i;
        }
        return null;
    }

    public boolean update(){
        String key = userId + DB_SUFFIX;
        return BucketManager.get(BUCKET_ID).set(key, this.toJSON());
    }

    private String toJSON(){
        return Json.toJsonPretty(this);
    }

    public String getBucketId() {
        return bucketId;
    }

    public int getUserId() {
        return userId;
    }

    public int getLevel() {
        return level;
    }

    public int getExp() {
        return exp;
    }
}
