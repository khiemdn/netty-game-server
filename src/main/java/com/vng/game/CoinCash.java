package com.vng.game;

import firebat.db.memcached.BucketManager;

public class CoinCash {
    private static final String DB_SUFFIX = "_coincash";

    private static long decode(String coins){
        long c;
        try {
            c = Long.parseLong(coins.trim());
        } catch (NumberFormatException e) {
            c = -1;
        }
        return c;
    }

    public static long add(String bucketId, int userId){
        String key = userId + DB_SUFFIX;
        if (BucketManager.get(bucketId).add(key, "0")) {
            return 0;
        } else {
            return -1;
        }
    }

    public static long get(String bucketId, int userId){
        String key = userId + DB_SUFFIX;
        return decode((String) BucketManager.get(bucketId).get(key));
    }

    public static long increase(String bucketId, int userId, long amount){
        String key = userId + DB_SUFFIX;
        return BucketManager.get(bucketId).increment(key, amount);
    }

    public static long decrease(String bucketId, int userId, long amount){
        String key = userId + DB_SUFFIX;
        return BucketManager.get(bucketId).decrement(key, amount);
    }
}
