package com.vng.game;

import firebat.db.memcached.BucketManager;

public final class UserCount {
    private static final String BUCKET_ID = "index";
    private static final String DB_KEY = "user_count";

    private String count;

    public static synchronized void increase(){
        BucketManager.get(BUCKET_ID).increment(DB_KEY, 1);
    }

    public static int Load(){
        Object value = BucketManager.get(BUCKET_ID).get(DB_KEY);
        if (value == null){
            if (BucketManager.get(BUCKET_ID).add(DB_KEY, "0")){
                return 0;
            }
            return -1;
        } else {
            try {
                return Integer.parseInt((String) value);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return -1;
            }
        }
    }
}
