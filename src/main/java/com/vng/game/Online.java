package com.vng.game;

import com.google.gson.Gson;
import firebat.db.memcached.BucketManager;

public class Online {
    private static final String BUCKET_ID = "cache";
    private static final String DB_KEY_SUFFIX = "_online";

    private String ip;
    private String chatKey;

    public static boolean add(int userId, String ip){
        Online o = new Online();
        o.ip = ip;
        String key = userId + DB_KEY_SUFFIX;
        return BucketManager.get(BUCKET_ID).add(key, o.toJson());
    }

    public static boolean remove(int userId){
        String key = userId + DB_KEY_SUFFIX;
        return BucketManager.get(BUCKET_ID).delete(key);
    }

    private String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
