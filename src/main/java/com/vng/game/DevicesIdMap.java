package com.vng.game;

import com.couchbase.client.vbucket.config.Bucket;
import firebat.db.memcached.BucketManager;

public class DevicesIdMap {
    private static final String BUCKET_ID = "index";
    private static final String DB_KEY_SUFFIX = "_device_id";

    private int userId;

    public static boolean add(String deviceId, int userId){
        String key = deviceId + DB_KEY_SUFFIX;
        return BucketManager.get(BUCKET_ID).add(key, userId);
    }

    public static int get(String deviceId){
        String key = deviceId + DB_KEY_SUFFIX;
        Object value = BucketManager.get(BUCKET_ID).get(key);
        if (value == null){
            return -1;
        }
        return Integer.parseInt(value.toString());
    }

    public static void reMap(String deviceId, int userId){
        String key = deviceId + DB_KEY_SUFFIX;
        Object value = BucketManager.get(BUCKET_ID).get(key);
        if (value == null){
            DevicesIdMap.add(deviceId, userId);
            return;
        }
        BucketManager.get(BUCKET_ID).set(key, userId);
    }
}
