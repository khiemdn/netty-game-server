package com.vng.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

public class ClientInitializer extends ChannelInitializer<SocketChannel> {
    final static String PIPELINE_IDLE = "idle";

    final static int IDLE_TIME_READER = 0; //second
    final static int IDLE_TIME_WRITER = 0; //second
    final static int IDLE_TIME_ALL = 60; //second

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline p = ch.pipeline();
        p.addLast(PIPELINE_IDLE, new IdleStateHandler(IDLE_TIME_READER, IDLE_TIME_WRITER, IDLE_TIME_ALL));
        p.addLast(new ClientDecoder());
        p.addLast(new ClientHandler());
    }
}
