package com.vng.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import static com.vng.Constant.Constant.DATA_SIZE_LENGTH;
import static com.vng.Constant.Constant.MAGIC_BYTE_ACCEPT;
import static com.vng.Constant.Constant.MAX_CLIENT_PACKAGE_SIZE;

public class ClientDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        in.markReaderIndex();
        byte magic = in.readByte();
        if (magic != MAGIC_BYTE_ACCEPT){
            ctx.close();
            return;
        }
        if (in.readableBytes() < DATA_SIZE_LENGTH){
            return;
        }
        int dataLength = in.readInt();
        if (dataLength <= 0 || dataLength >= MAX_CLIENT_PACKAGE_SIZE){
            ctx.close();
            return;
        }
        if (dataLength > in.readableBytes()){
            in.resetReaderIndex();
        } else {
            out.add(in.readBytes(dataLength));
        }
    }
}
