package com.vng.client;

import com.google.gson.reflect.TypeToken;
import com.google.protobuf.GeneratedMessageV3;
import com.vng.client.chat.ChatClient;
import com.vng.game.params.level.LevelInfo;
import com.vng.game.params.tree.TreeInfo;
import com.vng.proto.request.RequestCommand;
import com.vng.proto.response.ResponseCommand;
import firebat.log.Log;
import firebat.util.Hash;
import firebat.util.Json;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static com.vng.Constant.Constant.*;

public class ClientHandler extends ChannelInboundHandlerAdapter {
    public static ArrayList<LevelInfo> levelInfos;
    public static ConcurrentHashMap<String, TreeInfo> trees;
    private Random r = new Random();

    private int userId = 0;
    private String deviceId = "d-tt1223";
    private String osPlatform = "windows";
    private String osVersion = "10";
    private String keyChat = "";
    private long timeLogin = 0;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String levelsConfig = new String(Files.readAllBytes(Paths.get("./config/levels.json")));
        String treesConfig = new String(Files.readAllBytes(Paths.get("./config/trees.json")));
        Type levelsType = new TypeToken<ArrayList<LevelInfo>>(){}.getType();
        Type treesType = new TypeToken<ConcurrentHashMap<String, TreeInfo>>(){}.getType();
        levelInfos = Json.gson.fromJson(levelsConfig, levelsType);
        levelInfos.sort((o1, o2) -> o1.getLevel() - o2.getLevel());
        trees = Json.gson.fromJson(treesConfig, treesType);

        RequestCommand.LoginRequest req = RequestCommand.LoginRequest.newBuilder()
                .setUserId(userId)
                .setDeviceId(deviceId)
                .setOsPlatform(osPlatform)
                .setOsVersion(osVersion)
                .setHash(Hash.hash("SHA-256", HASH_KEY, userId, deviceId, osPlatform, osVersion))
                .build();

        ctx.writeAndFlush(getByteBuf(LOGIN_COMMAND_ID, DEFAULT_MESSAGE_TYPE, req));
    }

    public static ByteBuf getByteBuf (short cmdId, int msgType, GeneratedMessageV3 msg) throws Exception
    {
        int capacity = MAGIC_BYTES_LENGTH + DATA_SIZE_LENGTH + COMMAND_ID_LENGTH + MSG_TYPE_LENGTH + msg.getSerializedSize();
        ByteBuf buf = PooledByteBufAllocator.DEFAULT.ioBuffer(capacity);
        buf.writeByte(MAGIC_BYTE_ACCEPT);
        buf.writeInt(capacity - MAGIC_BYTES_LENGTH - DATA_SIZE_LENGTH);
        buf.writeShort(cmdId);
        buf.writeInt(msgType);
        msg.writeTo(new ByteBufOutputStream(buf));
        return buf;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf m = (ByteBuf) msg;
        short cmdId = m.readShort();
        int msgType = m.readInt();
        switch (cmdId){
            case LOGIN_COMMAND_ID:
                ResponseCommand.LoginResponse res = ResponseCommand.LoginResponse.parseFrom(new ByteBufInputStream(m));
                Log.debug(res.toString());
                userId = res.getUserId();
                keyChat = res.getKeyChat();
                timeLogin = res.getTime();
                ClientMain.chatClient = new ChatClient("localhost", 8080, userId, deviceId, keyChat, timeLogin);
                ClientMain.chatClient.run();
//                RequestCommand.BuyCoin buyCoin = RequestCommand.BuyCoin.newBuilder()
//                        .setUserId(userId)
//                        .setDeviceId(deviceId)
//                        .setAmount(100)
//                        .setHash(Hash.hash("SHA-256", HASH_KEY, userId, deviceId, 100))
//                        .build();
//                ctx.writeAndFlush(getByteBuf(BUY_COIN_COMMAND_ID, 1, buyCoin));
                break;
            case BUY_COIN_COMMAND_ID:
                ResponseCommand.BuyCoinResponse buyCoinResponse = ResponseCommand.BuyCoinResponse.parseFrom(new ByteBufInputStream(m));
                Log.debug(buyCoinResponse);
                int treeIndex = r.nextInt(trees.size());
                String treeName = (String) trees.keySet().toArray()[treeIndex];
                RequestCommand.PlantTree plantReq = RequestCommand.PlantTree.newBuilder()
                        .setUserId(userId)
                        .setDeviceId(deviceId)
                        .setTreeName(treeName)
                        .setSpentCoin(true)
                        .setHash(Hash.hash("SHA-256", HASH_KEY, userId, deviceId, treeName, true))
                        .build();
                ctx.writeAndFlush(getByteBuf(PLANT_COMMAND_ID, 1, plantReq));
                break;
            case BUY_GOLD_COMMAND_ID:
                break;
            case PLANT_COMMAND_ID:
                ResponseCommand.PlantTreeResponse plantTreeResponse = ResponseCommand.PlantTreeResponse.parseFrom(new ByteBufInputStream(m));
                Log.debug(plantTreeResponse);
                String treePlanted = plantTreeResponse.getTreeName();
                RequestCommand.HarvestTree harvestTree = RequestCommand.HarvestTree.newBuilder()
                        .setUserId(userId)
                        .setDeviceId(deviceId)
                        .setTreeName(treePlanted)
                        .setTreeUid(plantTreeResponse.getTreeUid())
                        .setHash(Hash.hash("SHA-256", HASH_KEY, userId, deviceId, treePlanted))
                        .build();
                ctx.writeAndFlush(getByteBuf(HARVEST_COMMAND_ID, 1, harvestTree));
                break;
            case HARVEST_COMMAND_ID:
                ResponseCommand.HarvestResponse harvestResponse = ResponseCommand.HarvestResponse.parseFrom(new ByteBufInputStream(m));
                Log.debug(harvestResponse);
                long coins = harvestResponse.getCoinCash() + harvestResponse.getCoinBonus();
                if (coins >= 10){
                    RequestCommand.PlantTree plantTreeReq = MakePlantTreeRequest();
                    ctx.writeAndFlush(getByteBuf(PLANT_COMMAND_ID, 1, plantTreeReq));
                }
                break;
            default:
        }
    }

    private RequestCommand.PlantTree MakePlantTreeRequest() throws Exception{
        int treeIndex = r.nextInt(trees.size());
        String treeName = (String) trees.keySet().toArray()[treeIndex];
        return RequestCommand.PlantTree.newBuilder()
                .setUserId(userId)
                .setDeviceId(deviceId)
                .setTreeName(treeName)
                .setSpentCoin(true)
                .setHash(Hash.hash("SHA-256", HASH_KEY, userId, deviceId, treeName, true))
                .build();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        super.userEventTriggered(ctx, evt);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
