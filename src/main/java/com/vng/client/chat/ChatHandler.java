package com.vng.client.chat;

import com.vng.client.ClientMain;
import firebat.log.Log;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.CharsetUtil;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ChatHandler extends SimpleChannelInboundHandler<ByteBuf> {

    ScheduledFuture sf;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        if (ChatClient.ctx.channel().isActive()){
            ChatClient.ctx.close().sync();
        }
        ChatClient.ctx = ctx;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        String m = msg.toString(CharsetUtil.UTF_8);
        Log.debug(m);
        if (sf == null) {
            sf = ctx.channel().eventLoop().scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    String ms = "woooo";
                    ByteBuf r = ctx.alloc().ioBuffer();
                    r.writeInt(ms.getBytes().length + 1);
                    r.writeByte(3);
                    r.writeCharSequence("woooooo", CharsetUtil.UTF_8);
                    ctx.writeAndFlush(r);
                }
            }, 0, 3, TimeUnit.SECONDS);
        }
    }
}
