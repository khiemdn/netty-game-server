package com.vng.client.chat;

import com.vng.proto.request.RequestCommand;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.CharsetUtil;

public class ChatClient {
    private int room;
    private int userId;
    private String deviceId;
    private String keyChat;
    private long timeLogin;
    public static ChannelHandlerContext ctx;
    private String host;
    private int port;

    public ChatClient(String host, int port, int userId, String deviceId, String keyChat, long timeLogin) {
        this.host = host;
        this.port = port;
        this.userId  = userId;
        this.keyChat = keyChat;
        this.timeLogin = timeLogin;
        this.deviceId = deviceId;
    }

    public void run(){
        Bootstrap b = new Bootstrap()
                .group(firebat.io.ShareLoopGroup.worker())
                .channel(NioSocketChannel.class)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new ChatHandler());
                    }
                });
        b.connect(host, port).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                RequestCommand.JoinChatRoom joinChatRoom = RequestCommand.JoinChatRoom.newBuilder()
                        .setUserId(userId)
                        .setDeviceId(deviceId)
                        .setKeyChat(keyChat)
                        .setTimeLogin(timeLogin)
                        .build();
                ByteBuf m = PooledByteBufAllocator.DEFAULT.ioBuffer();
                m.writeInt(1 + joinChatRoom.getSerializedSize());
                m.writeByte(0);
                joinChatRoom.writeTo(new ByteBufOutputStream(m));
                channelFuture.channel().writeAndFlush(m);
            }
        });
    }

    public void sendMsg(String msg){
        ByteBuf m = PooledByteBufAllocator.DEFAULT.ioBuffer();
        m.writeInt(1 + msg.getBytes().length);
        m.writeByte(1);
        m.writeCharSequence(msg, CharsetUtil.UTF_8);
        ctx.writeAndFlush(m);
    }
}
