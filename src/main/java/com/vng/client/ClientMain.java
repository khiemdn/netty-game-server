package com.vng.client;

import com.vng.client.chat.ChatClient;
import firebat.io.socket.SocketClientAbstractInitializer;
import firebat.io.socket.SocketClient;
import firebat.io.udp.Udp;
import firebat.log.Log;
import firebat.util.Environment;
import firebat.util.ServiceStatus;
import io.netty.channel.socket.SocketChannel;

/**
 * Created by thuanvt on 12/11/2014.
 */
public class ClientMain {
    private final static ServiceStatus status = new ServiceStatus(true);
    private static Environment environment;
    private static SocketClientAbstractInitializer socketClientInitializer;
    private static SocketChannel socketHandler;

    public static ChatClient chatClient;

    public static void main (String[] args) {
        try {
            status.setStartingUp();
            addShutdownHook();
            if (args != null && args.length > 0) {
                environment = Environment.valueOf(args[0]);
            } else {
                environment = Environment.DEV;
            }
            Log.start("./config/log.properties", true);

            SocketClient.setWaterMark(32000, 8000);
            SocketClient.setBuf(1000000, 1000000);

            socketClientInitializer = new SocketClientAbstractInitializer() {
                @Override
                public void connectFail() {

                }

                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    ClientInitializer cs = new ClientInitializer();
                    cs.initChannel(ch);
                    socketHandler = ch;
                }
            };
            SocketClient.connect(null, "127.0.0.1", 8030, 3000, socketClientInitializer);

            status.setStarted();
        } catch (Exception e) {
            Log.exception(e);
        }
    }

    public static synchronized void stop() throws Exception {
        status.setShuttingDown();

        if (socketHandler != null)
            socketHandler.close();

        Log.stop();
        status.setShutdown();
    }

    private static void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    ClientMain.stop();
                } catch (Exception e) {
                    Log.exception(e);
                }
            }
        });
    }

    public static boolean isRunning () {
        return status.isStarted();
    }
}
