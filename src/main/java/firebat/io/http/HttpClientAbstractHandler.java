package firebat.io.http;

import firebat.log.Log;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.timeout.IdleStateEvent;


/**
 * Created by thuanvt on 11/11/2014.
 */
public abstract class HttpClientAbstractHandler extends ChannelInboundHandlerAdapter implements ChannelFutureListener{
    public HttpRequest request;

    public abstract void connectFail ();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(request);
    }

    @Override
    public void exceptionCaught (ChannelHandlerContext ctx, Throwable cause) throws Exception
    {
        ctx.close();
        Log.exception(cause);
    }

    @Override
    public void userEventTriggered (ChannelHandlerContext ctx, Object evt) throws Exception
    {
        if (evt instanceof IdleStateEvent)
        {
            ctx.close();
        }
    }

    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isDone() == false || future.isSuccess() == false)
            connectFail();
    }
}
