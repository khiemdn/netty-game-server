package firebat.io.websocket;

import firebat.io.BootstrapInfo;
import firebat.io.ShareLoopGroup;
import firebat.log.Log;
import firebat.util.Address;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;


public class WebSocketServer
{
    private String          id;
    private String          host;
    private int             port;
    private ServerBootstrap bootstrap;
    private EventLoopGroup  bossGroup, workerGroup;
    private Channel channel;

    private int highWaterMark;
    private int lowWaterMark;
    private int sndBuf, rcvBuf;
    private int backlog;
    private int numBossThread, numWorkerThread;


    public WebSocketServer (String id)
    {
        this.id = id;
        bootstrap = new ServerBootstrap()
                .channel(NioServerSocketChannel.class)
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.TCP_NODELAY, true);
    }

    public WebSocketServer (String id, BootstrapInfo info)
    {
        this(id);

        host = info.getHost();
        port = info.getPort();
        highWaterMark = info.getHighWaterMark();
        lowWaterMark = info.getLowWaterMark();
        sndBuf = info.getSndBuf();
        rcvBuf = info.getRcvBuf();
        backlog = info.getBacklog();
        numBossThread = info.getNumBossThread();
        numWorkerThread = info.getNumWorkerThread();
    }

    public ServerBootstrap getBootstrap ()
    {
        return bootstrap;
    }

    public synchronized boolean start (String host, int port, ChannelInitializer<SocketChannel> initializer) throws Exception
    {
        if (bossGroup != null || workerGroup != null)
            return false;

        this.host = host;
        this.port = port;
        return start(initializer);
    }

    public synchronized boolean start (ChannelInitializer<SocketChannel> initializer) throws Exception
    {
        if (bossGroup != null || workerGroup != null)
            return false;

        if (numBossThread < 0)
            bossGroup = ShareLoopGroup.boss();
        else if (numBossThread == 0)
            bossGroup = new NioEventLoopGroup();
        else
            bossGroup = new NioEventLoopGroup(numBossThread);

        if (numWorkerThread < 0)
            workerGroup = ShareLoopGroup.worker();
        else if (numWorkerThread == 0)
            workerGroup = new NioEventLoopGroup();
        else
            workerGroup = new NioEventLoopGroup(numWorkerThread);

        if (highWaterMark > 0 && lowWaterMark > 0)
            bootstrap.childOption(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(lowWaterMark, highWaterMark));
        if (sndBuf > 0)
            bootstrap.childOption(ChannelOption.SO_SNDBUF, sndBuf);
        if (rcvBuf > 0)
            bootstrap.childOption(ChannelOption.SO_RCVBUF, rcvBuf);
        if (backlog > 0)
            bootstrap.option(ChannelOption.SO_BACKLOG, backlog);

        channel = bootstrap.group(bossGroup, workerGroup)
                 .childHandler(initializer)
                 .bind(Address.getInetSocketAddress(host, port))
                 .sync()
                 .channel();

        Log.info("WebSocketServer", "start", id, host + ":" + port);
        return true;
    }


    public synchronized boolean stop ()
    {
        if (bossGroup == null || workerGroup == null)
            return false;

        Log.info("WebSocketServer", "stop", id, host + ":" + port);
        channel.close();
        bossGroup = null;
        workerGroup = null;
        return true;
    }

    public WebSocketServer setWaterMark (int highWaterMark, int lowWaterMark)
    {
        this.highWaterMark = highWaterMark;
        this.lowWaterMark = lowWaterMark;
        return this;
    }

    public WebSocketServer setBuf (int sndBuf, int rcvBuf)
    {
        this.sndBuf = sndBuf;
        this.rcvBuf = rcvBuf;
        return this;
    }

    public WebSocketServer setBacklog (int backlog)
    {
        this.backlog = backlog;
        return this;
    }

    public WebSocketServer setNumThread (int numBossThread, int numWorkerThread)
    {
        this.numBossThread = numBossThread;
        this.numWorkerThread = numWorkerThread;
        return this;
    }
}
