package firebat.io;

import firebat.io.http.HttpClient;
import firebat.io.http.HttpClientHandler;
import firebat.io.socket.SocketClientAbstractInitializer;
import firebat.io.socket.SocketClientDecoder;
import firebat.io.socket.SocketClientHandler;
import firebat.io.socket.SocketClient;
import firebat.log.Log;
import firebat.util.Environment;
import firebat.util.ServiceStatus;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * Created by thuanvt on 12/11/2014.
 */
public class DemoClient {
private final static ServiceStatus status = new ServiceStatus(true);
    private static Environment environment;
    private static SocketClientAbstractInitializer socketClientInitializer;
    private static SocketClientHandler socketHandler;

    public static void main (String[] args) {
        try {
            status.setStartingUp();
            addShutdownHook();
            if (args != null && args.length > 0) {
                environment = Environment.valueOf(args[0]);
            } else {
                environment = Environment.DEV;
            }
            Log.start("./config/log.properties", true);

//            HttpClient.initSsl();
//            HttpClient.setBuf(1000000, 1000000);

//            HttpClient.sendHttpRequest(
//                    "https://graph.facebook.com/oauth/access_token",
//                    new HttpClientHandler(),
//                    5000,
//                    5000);

//            HttpClient.sendHttpRequest(null,
//                    "http://127.0.0.1/test.php",
//                    new HttpClientHandler(),
//                    5000,
//                    5000);

            SocketClient.setWaterMark(32000, 8000);
            SocketClient.setBuf(1000000, 1000000);

            socketHandler = new SocketClientHandler();
            socketClientInitializer = new SocketClientAbstractInitializer() {
                @Override
                public void connectFail() {

                }

                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    ChannelPipeline pipeline = ch.pipeline();
                    pipeline.addLast(new SocketClientDecoder());
                    pipeline.addLast(socketHandler);
                }
            };
            SocketClient.connect(null, "127.0.0.1", 8030, 3000, socketClientInitializer);

            status.setStarted();
        } catch (Exception e) {
            Log.exception(e);
        }
    }

    public static synchronized void stop() throws Exception {
        status.setShuttingDown();

        if (socketHandler != null)
            socketHandler.close();

        Log.stop();
        status.setShutdown();
    }

    private static void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    DemoClient.stop();
                } catch (Exception e) {
                    Log.exception(e);
                }
            }
        });
    }

    public static boolean isRunning () {
        return status.isStarted();
    }
}
