package firebat.io;

/**
 * Created by CPU01736-local on 3/10/2015.
 */
public class BootstrapInfo
{
    private String host;
    private int port;
    private Integer highWaterMark;
    private Integer lowWaterMark;
    private Integer sndBuf, rcvBuf;
    private Integer backlog;
    private Integer numBossThread, numWorkerThread;

    public int getBacklog ()
    {
        return (backlog == null) ? 0 : backlog;
    }

    public void setBacklog (int backlog)
    {
        this.backlog = backlog;
    }

    public int getHighWaterMark ()
    {
        return (highWaterMark == null) ? 0 : highWaterMark;
    }

    public void setHighWaterMark (int highWaterMark)
    {
        this.highWaterMark = highWaterMark;
    }

    public String getHost ()
    {
        return host;
    }

    public void setHost (String host)
    {
        this.host = host;
    }

    public int getLowWaterMark ()
    {
        return (lowWaterMark == null) ? 0 : lowWaterMark;
    }

    public void setLowWaterMark (int lowWaterMark)
    {
        this.lowWaterMark = lowWaterMark;
    }

    public int getNumBossThread ()
    {
        return (numBossThread == null) ? 0 : numBossThread;
    }

    public void setNumBossThread (int numBossThread)
    {
        this.numBossThread = numBossThread;
    }

    public int getNumWorkerThread ()
    {
        return (numWorkerThread == null) ? 0 : numWorkerThread;
    }

    public void setNumWorkerThread (int numWorkerThread)
    {
        this.numWorkerThread = numWorkerThread;
    }

    public int getPort ()
    {
        return port;
    }

    public void setPort (int port)
    {
        this.port = port;
    }

    public int getRcvBuf ()
    {
        return (rcvBuf == null) ? 0 : rcvBuf;
    }

    public void setRcvBuf (int rcvBuf)
    {
        this.rcvBuf = rcvBuf;
    }

    public int getSndBuf ()
    {
        return (sndBuf == null) ? 0 : sndBuf;
    }

    public void setSndBuf (int sndBuf)
    {
        this.sndBuf = sndBuf;
    }
}
