package firebat.io.socket;

import com.google.common.primitives.Chars;
import firebat.log.Log;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;

import java.util.concurrent.TimeUnit;

/**
 * Created by thuanvt on 12/10/2014.
 */
public class SocketClientHandler extends ChannelInboundHandlerAdapter
{
    private ChannelHandlerContext ctx;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Log.debug("[ACTIVE]");
        this.ctx = ctx;
        ByteBuf login = ctx.alloc().heapBuffer();
        login.writeCharSequence("LOGIN 1", CharsetUtil.UTF_8);
        ByteBuf loginMSG = ctx.alloc().ioBuffer().writeInt(login.readableBytes());
        loginMSG.writeBytes(login);
        ctx.writeAndFlush(loginMSG);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Log.debug("[INACTIVE]");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf m = (ByteBuf) msg;
        // writeAndFlush((ByteBuf) msg);
        String mStr = m.toString(CharsetUtil.UTF_8);
        System.out.println(mStr);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        Log.exception(cause);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            Log.debug("[IDLE]");
            ctx.close();
        }
    }

    public void writeAndFlush(byte[] data) {
        int len = data.length;
        ctx.writeAndFlush(ctx.alloc().buffer(len + SocketServerDecoder.HEADER_LEN)
                .writeInt(len)
                .writeBytes(data));
    }

    public void writeAndFlush(ByteBuf data) {
        int len = data.readableBytes();
        ctx.writeAndFlush(ctx.alloc().buffer(len + SocketServerDecoder.HEADER_LEN)
                .writeInt(len)
                .writeBytes(data));
    }

    public void close() {
        if (ctx != null)
            ctx.close();
    }

    public void removeIdleTime() {
        ChannelPipeline pipeline = ctx.pipeline();
        if (pipeline.get(SocketClientInitializer.PIPELINE_IDLE) != null)
            pipeline.remove(SocketClientInitializer.PIPELINE_IDLE);
    }

    public void setIdleTime(int idleTimeReader, int idleTimeWriter, int idleTimeAll) {
        removeIdleTime();
        ctx.pipeline().addFirst(SocketClientInitializer.PIPELINE_IDLE, new IdleStateHandler(idleTimeReader, idleTimeWriter, idleTimeAll, TimeUnit.MILLISECONDS));
    }
}
