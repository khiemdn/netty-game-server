package firebat.io;

import firebat.db.memcached.BucketManager;
import firebat.io.http.HttpServer;
import firebat.io.http.HttpServerInitializer;
import firebat.io.socket.SocketServer;
import firebat.io.socket.SocketServerInitializer;
import firebat.io.udp.Udp;
import firebat.io.udp.UdpHandler;
import firebat.io.websocket.WebSocketServer;
import firebat.io.websocket.WebSocketServerInitializer;
import firebat.log.Log;
import firebat.util.Environment;
import firebat.util.ServiceStatus;

import java.util.concurrent.TimeUnit;

/**
 * Created by thuanvt on 12/11/2014.
 */
public class DemoServer {
    private final static ServiceStatus status = new ServiceStatus(true);
    private static Environment environment;
    private static SocketServer socket;
    private static WebSocketServer webSocket;
    private static HttpServer http;
    private static Udp udp;

    public static void main (String[] args) {
        try {
            status.setStartingUp();
            addShutdownHook();
            if (args != null && args.length > 0) {
                environment = Environment.valueOf(args[0]);
            } else {
                environment = Environment.DEV;
            }
            Log.start("./config/log.properties", true);
//            BucketManager.start("./data/memcachedDev.json");

            socket = new SocketServer("demoSocket")
//                    .setNumThread(8, 8)
//                    .setWaterMark(32000, 8000)
//                    .setBacklog(128)
//                    .setBuf(1000000, 1000000)
            ;
            socket.start("*", 8030, new SocketServerInitializer());

            http = new HttpServer("demoHttp")
//                    .setNumThread(8, 8)
//                    .setWaterMark(32000, 8000)
//                    .setBacklog(128)
//                    .setBuf(1000000, 1000000)
            ;
            http.start("*", 8031, new HttpServerInitializer());

            udp = new Udp("demoUdp")
//                    .setBuf(10000000, 10000000)
            ;
            udp.start("*", 8032, new UdpHandler());

            webSocket = new WebSocketServer("demoWebSocket")
//                    .setNumThread(8, 8)
//                    .setWaterMark(32000, 8000)
//                    .setBacklog(128)
//                    .setBuf(1000000, 1000000)
            ;
            webSocket.start("*", 8033, new WebSocketServerInitializer());

            status.setStarted();
        } catch (Exception e) {
            Log.exception(e);
        }
    }

    public static synchronized void stop() throws Exception {
        status.setShuttingDown();

        if (socket != null)
            socket.stop();
        if (http != null)
            http.stop();
        if (udp != null)
            udp.stop();
        if (webSocket != null)
            webSocket.stop();

        Log.stop();
        status.setShutdown();
    }

    private static void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    DemoServer.stop();
                } catch (Exception e) {
                    Log.exception(e);
                }
            }
        });
    }

    public static boolean isRunning () {
        return status.isStarted();
    }
}
