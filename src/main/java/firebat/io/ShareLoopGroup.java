package firebat.io;

import io.netty.channel.nio.NioEventLoopGroup;

import java.util.concurrent.*;

/**
 * Created by CPU01736-local on 3/11/2015.
 */
public class ShareLoopGroup
{
    private static NioEventLoopGroup BOSS;
    private static NioEventLoopGroup WORKER = new NioEventLoopGroup();

    public static synchronized NioEventLoopGroup boss()
    {
        if (BOSS == null)
            BOSS = new NioEventLoopGroup();
        return BOSS;
    }

    public static NioEventLoopGroup worker()
    {
        return WORKER;
    }

    public final static ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit)
    {
        return WORKER.schedule(command, delay, unit);
    }

    public final static <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit)
    {
        return WORKER.schedule(callable, delay, unit);
    }

    public final static ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit)
    {
        return WORKER.scheduleAtFixedRate(command, initialDelay, period, unit);
    }

    public final static ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit)
    {
        return WORKER.scheduleWithFixedDelay(command, initialDelay, delay, unit);
    }
}
