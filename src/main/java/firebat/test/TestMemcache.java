package firebat.test;

import firebat.db.memcached.AbstractDbKeyValue;
import firebat.db.memcached.BucketManager;
import firebat.log.Log;

import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by CPU10399-local on 6/5/2015.
 */
public class TestMemcache
{
    public static void main (String[] args)
    {
        try
        {
            BucketManager.start("./data/memcachedServerTest.json");
            int numThread = Runtime.getRuntime().availableProcessors();

            test(BucketManager.get("index"), numThread);
            test(BucketManager.get("cache"), numThread);
        }
        catch (Exception e)
        {
            Log.exception(e);
        }
        finally
        {
            BucketManager.stop();
        }
    }

    public static void test (AbstractDbKeyValue bucket, int numThread) throws Exception
    {
        testNumKey(bucket, numThread);
    }

    public static void testNumKey (AbstractDbKeyValue bucket, int numThread) throws Exception
    {
        final String KEY_NAME = "TEST_NUM_KEY";
        final int numTest = 20000;
        Log.debug("testNumKey", KEY_NAME, numThread, numTest);

        bucket.set(KEY_NAME, "0");

        AtomicLong totalTime = new AtomicLong();
        ArrayBlockingQueue<Long> listResult = new ArrayBlockingQueue<>(numThread * numTest);
        CyclicBarrier startBarrier = new CyclicBarrier(numThread);
        CountDownLatch endLatch = new CountDownLatch(numThread);
        for (int i = 0; i < numThread; i++)
        {
            final int id = i;
            new Thread(new Runnable() {
                @Override
                public void run ()
                {
                    try
                    {
                        long[] result = new long[numTest];
                        startBarrier.await();
                        long time = System.currentTimeMillis();
                        for (int j = 0; j < numTest; j++)
                        {
                            result[j] = bucket.increment(KEY_NAME, 1);
                        }
                        totalTime.addAndGet(System.currentTimeMillis() - time);
                        for (long v : result)
                            listResult.add(v);
                        endLatch.countDown();
                    }
                    catch (Exception e)
                    {
                        Log.exception(e);
                    }
                }
            }).start();
        }
        endLatch.await();

        long totalTest = numTest * numThread;
        long realResult = Long.parseLong((String) bucket.get(KEY_NAME));
        long expectResult = totalTest;
        Log.debug("Result", (realResult == expectResult) ? "MATCH" : "NOT MATCH", realResult, expectResult);

        int numDuplicate = 0;
        HashSet<Long> setResult = new HashSet<>();
        for (long v : listResult)
            if (setResult.add(v) == false)
            {
                numDuplicate++;
                Log.debug("duplicate num", v);
            }
        Log.debug("numDuplicate", numDuplicate);
        Log.debug("rate", (totalTime.get() / totalTest) * 1000);
    }
}
