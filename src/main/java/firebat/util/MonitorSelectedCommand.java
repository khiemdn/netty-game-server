package firebat.util;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by CPU10399-local on 1/7/2016.
 */
public class MonitorSelectedCommand<K> extends MonitorCommand<K>
{
    private ConcurrentHashMap<K, InfoAdder<K>> infos = new ConcurrentHashMap<>();

    public boolean addCommand (K id)
    {
        return infos.putIfAbsent(id, new InfoAdder<>(id)) == null;
    }

    @Override
    public HashMap<K, Info> snapshot (boolean isReset)
    {
        HashMap<K, Info> map = new HashMap<>(infos.size());
        for (InfoAdder<K> i : infos.values())
        {
            Info snapshot = i.snapshot(isReset);
            if (snapshot.inUsed())
                map.put(i.id, snapshot);
        }
        return map;
    }

    @Override
    public boolean read (K id, int size)
    {
        InfoAdder i = infos.get(id);
        if (i != null)
        {
            i.numRead.increment();
            i.sizeRead.add(size);
            return true;
        }
        return false;
    }

    @Override
    public boolean handle (K id, long time)
    {
        InfoAdder i = infos.get(id);
        if (i != null)
        {
            i.numHandle.increment();
            i.timeHandle.add(time);
            return true;
        }
        return false;
    }

    @Override
    public boolean write (K id, int size)
    {
        InfoAdder i = infos.get(id);
        if (i != null)
        {
            i.numWrite.increment();
            i.sizeWrite.add(size);
            return true;
        }
        return false;
    }
}
