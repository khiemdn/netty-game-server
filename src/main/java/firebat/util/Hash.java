package firebat.util;

import org.apache.commons.codec.binary.Hex;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class Hash
{
    public static StringBuilder toStringBuilder (Object... ao)
    {
        StringBuilder sb = new StringBuilder(256);
        for (Object o : ao)
        {
            if (o != null)
                sb.append(o);
        }
        return sb;
    }

    public static String toString (Object... ao)
    {
        return toStringBuilder(ao).toString();
    }

    public static byte[] toByteArray (Charset charset, Object... ao)
    {
        if (charset == null)
            return toString(ao).getBytes(StandardCharsets.UTF_8);
        else
            return toString(ao).getBytes(charset);
    }
    
    //algorithm: MD5, SHA-1, SHA-256    
    public static String hash (String algorithm, Object... ao) throws Exception
    {
        return hash(algorithm, toByteArray(null, ao));
    }
    
    public static String hash (String algorithm, byte[] data) throws Exception
    {
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        return Hex.encodeHexString(messageDigest.digest(data));
    }
    
    public static int murmurHash3_x86_32 (int seed, Object... ao)
    {
        return murmurHash3_x86_32(seed, toByteArray(null, ao));
    }
    
    public static int murmurHash3_x86_32 (int seed, byte[] data)
    {
        int len = data.length;

        final int c1 = 0xcc9e2d51;
        final int c2 = 0x1b873593;

        int h1 = seed;
        int roundedEnd = (len & 0xfffffffc);  // round down to 4 byte block

        for (int i = 0; i < roundedEnd; i += 4)
        {
            // little endian load order
            int k1 = (data[i] & 0xff) | ((data[i + 1] & 0xff) << 8) | ((data[i + 2] & 0xff) << 16) | (data[i + 3] << 24);
            k1 *= c1;
            k1 = (k1 << 15) | (k1 >>> 17);  // ROTL32(k1,15);
            k1 *= c2;

            h1 ^= k1;
            h1 = (h1 << 13) | (h1 >>> 19);  // ROTL32(h1,13);
            h1 = h1 * 5 + 0xe6546b64;
        }

        // tail
        int k1 = 0;

        switch (len & 0x03)
        {
            case 3:
                k1 = (data[roundedEnd + 2] & 0xff) << 16;
            // fallthrough
            case 2:
                k1 |= (data[roundedEnd + 1] & 0xff) << 8;
            // fallthrough
            case 1:
                k1 |= (data[roundedEnd] & 0xff);
                k1 *= c1;
                k1 = (k1 << 15) | (k1 >>> 17);  // ROTL32(k1,15);
                k1 *= c2;
                h1 ^= k1;
        }

        // finalization
        h1 ^= len;

        // fmix(h1);
        h1 ^= h1 >>> 16;
        h1 *= 0x85ebca6b;
        h1 ^= h1 >>> 13;
        h1 *= 0xc2b2ae35;
        h1 ^= h1 >>> 16;

        return h1;
    }
}
