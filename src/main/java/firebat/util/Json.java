package firebat.util;

import com.google.gson.*;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Json
{
    public final static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create();

    public final static      Gson gsonPretty       = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .setPrettyPrinting()
            .create();
    private final static Gson gsonStatic       = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
            .create();
    private final static Gson gsonStaticPretty = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
            .setPrettyPrinting()
            .create();

    private final static JsonParser parser = new JsonParser();

    public static <T> T fromJson (String json, Class<T> classOfT, boolean includeStatic)
    {
        return (includeStatic ? gsonStatic : gson).fromJson(json, classOfT);
    }

    public static <T> T fromJson (String json, Class<T> classOfT)
    {
        return fromJson(json, classOfT, false);
    }

    public static <T> T fromFile (String filePath, Class<T> classOfT, boolean includeStatic) throws Exception
    {
        return (includeStatic ? gsonStatic : gson).fromJson(Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8), classOfT);
    }

    public static <T> T fromFile (String filePath, Class<T> classOfT) throws Exception
    {
        return fromFile(filePath, classOfT, false);
    }

    public static <T> T fromJson (JsonElement json, Class<T> classOfT, boolean includeStatic)
    {
        return (includeStatic ? gsonStatic : gson).fromJson(json, classOfT);
    }

    public static <T> T fromJson (JsonElement json, Class<T> classOfT)
    {
        return fromJson(json, classOfT, false);
    }

    public static <T> T fromJson (JsonElement json, Type type)
    {
        return gson.fromJson(json, type);
    }

    public static String toJson (Object src, boolean includeStatic)
    {
        return (includeStatic ? gsonStatic : gson).toJson(src);
    }

    public static String toJson (Object src)
    {
        return gson.toJson(src);
    }

    public static <T> String toJson (Object src,  Class<T> classOfT)
    {
        return gson.toJson(src, classOfT);
    }

    public static String toJsonPretty (Object src, boolean includeStatic)
    {
        return (includeStatic ? gsonStaticPretty : gsonPretty).toJson(src);
    }
    
    public static String toJsonPretty (Object src)
    {
        return gsonPretty.toJson(src);
    }

    public static JsonElement toJsonTree (Object src, boolean includeStatic)
    {
        return (includeStatic ? gsonStatic : gson).toJsonTree(src);
    }

    public static JsonElement toJsonTree (Object src)
    {
        return gson.toJsonTree(src);
    }

    public static JsonElement parse (String json)
    {
        return parser.parse(json);
    }

    public static JsonObject parseJsonObject (String json)
    {
        try
        {
            return parser.parse(json).getAsJsonObject();
        }
        catch (Exception e)
        {

        }
        return null;
    }
}
