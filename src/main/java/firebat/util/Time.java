package firebat.util;

import firebat.log.Log;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class Time
{
    public final static long ZONE_MILLIS_OFFSET = new GregorianCalendar().getTimeZone().getRawOffset();

    public final static int SECOND_IN_MINUTE = 60;
    public final static int SECOND_IN_HOUR   = 60 * SECOND_IN_MINUTE;
    public final static int SECOND_IN_DAY    = 24 * SECOND_IN_HOUR;
    public final static int SECOND_IN_7_DAY  = 7 * SECOND_IN_DAY;
    public final static int SECOND_IN_30_DAY = 30 * SECOND_IN_DAY;
    public final static int SECOND_IN_31_DAY = 31 * SECOND_IN_DAY;

    public final static long MILLISECOND_IN_MINUTE = 1000 * SECOND_IN_MINUTE;
    public final static long MILLISECOND_IN_HOUR   = 1000 * SECOND_IN_HOUR;
    public final static long MILLISECOND_IN_DAY    = 1000 * SECOND_IN_DAY;
    public final static long MILLISECOND_IN_7_DAY  = 1000 * SECOND_IN_7_DAY;
    public final static long MILLISECOND_IN_30_DAY = 1000 * SECOND_IN_30_DAY;
    public final static long MILLISECOND_IN_31_DAY = 1000 * SECOND_IN_31_DAY;

    public final static int SECOND_ZONE_OFFSET = ZonedDateTime.now().getOffset().getTotalSeconds();
    public final static int HOUR_ZONE_OFFSET = SECOND_ZONE_OFFSET / SECOND_IN_HOUR;
    public final static ZoneOffset ZONE_OFFSET = ZoneOffset.ofHours(HOUR_ZONE_OFFSET);

    /**
     * Lấy thời gian hiện tại theo đơn vị milli giây
     *
     * @return
     */
    public static long currentTimeMillis ()
    {
        return System.currentTimeMillis();
    }

    /**
     * Lấy thời gian hiện tại theo đơn vị giây
     *
     * @return
     */
    public static long currentTimeSecond ()
    {
        return System.currentTimeMillis() / 1000;
    }

    public static long currentTimeSecond (int deltaTime)
    {
        return System.currentTimeMillis() / 1000 + deltaTime;
    }

    public static long durationDays (long timeBegin, int deltaTime)
    {
        deltaTime += SECOND_ZONE_OFFSET;
        long dayBegin = TimeUnit.SECONDS.toDays(timeBegin + deltaTime);
        long dayCurrent = TimeUnit.SECONDS.toDays(Time.currentTimeSecond(deltaTime));
        return dayCurrent - dayBegin;

    }

    /**
     * Lấy thông tin thời gian hiện tại dạng chuỗi
     *
     * @param pattern ví dụ: HH:mm:ss dd/MM/yyyy
     * @return
     */
    public static String currentDateString (String pattern)
    {
        return new SimpleDateFormat(pattern).format(new Date());
    }

    public static String currentDateString ()
    {
        return LocalDateTime.now().toString();
    }

    /**
     * Lấy thông tin thời gian dạng chuỗi
     *
     * @param pattern      ví dụ: HH:mm:ss dd/MM/yyyy
     * @param milliseconds thời cần chuyển về dạng chuỗi
     * @return
     */
    public static String getDateString (String pattern, long milliseconds)
    {
        return new SimpleDateFormat(pattern).format(new Date(milliseconds));
    }

    public static LocalDateTime parseDateTime (String pattern, String input)
    {
        if (input != null || input.length() > 0)
        {
            try
            {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
                return LocalDateTime.parse(input, formatter);
            }
            catch (Exception e)
            {
                //Log.exception(e);
            }
        }
        return null;
    }

    public static long toUnixTime (String pattern, String input)
    {
        return toUnixTime(DateTimeFormatter.ofPattern(pattern), input);
    }

    public static long toUnixTime (DateTimeFormatter formatter, String input)
    {
        return LocalDateTime.parse(input, formatter).toEpochSecond(ZONE_OFFSET);
    }

    public static int toEpochDay (DateTimeFormatter formatter, String input)
    {
        return (int) LocalDate.parse(input, formatter).toEpochDay();
    }

    public static long toEpochDay (long unixTime)
    {
        return TimeUnit.SECONDS.toDays(unixTime);
    }


    public static long chooseTime (long time)
    {
        long curTime = Time.currentTimeSecond();
        return (time > 0 && time < curTime) ? time : curTime;
    }
}
