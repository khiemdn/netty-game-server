package firebat.util;

import java.util.HashMap;

/**
 * Created by CPU10399-local on 1/7/2016.
 */
public class MonitorSequenceCommand extends MonitorCommand<Integer>
{
    private int                  maxId;
    private InfoAdder<Integer>[] infos;

    @SuppressWarnings("unchecked")
    public MonitorSequenceCommand (int maxId)
    {
        this.maxId = maxId;
        infos = new InfoAdder[maxId];
        for (int i = 0; i < maxId; i++)
            infos[i] = new InfoAdder<>(i);
    }

    public boolean read (int id, int size)
    {
        if (id >= 0 && id < maxId)
        {
            InfoAdder<Integer> i = infos[id];
            i.numRead.increment();
            i.sizeRead.add(size);
            return true;
        }
        return false;
    }

    public boolean handle (int id, long time)
    {
        if (id >= 0 && id < maxId)
        {
            InfoAdder i = infos[id];
            i.numHandle.increment();
            i.timeHandle.add(time);
            return true;
        }
        return false;
    }

    public boolean write (int id, int size)
    {
        if (id >= 0 && id < maxId)
        {
            InfoAdder i = infos[id];
            i.numWrite.increment();
            i.sizeWrite.add(size);
            return true;
        }
        return false;
    }

    @Override
    public HashMap<Integer, Info> snapshot (boolean isReset)
    {
        HashMap<Integer, Info> map = new HashMap<>(infos.length);
        for (InfoAdder<Integer> i : infos)
        {
            Info snapshot = i.snapshot(isReset);
            if (snapshot.inUsed())
                map.put(i.id, snapshot);
        }
        return map;
    }

    @Override
    public boolean read (Integer id, int size)
    {
        return read(id.intValue(), size);
    }

    @Override
    public boolean handle (Integer id, long time)
    {
        return handle(id.intValue(), time);
    }

    @Override
    public boolean write (Integer id, int size)
    {
        return write(id.intValue(), size);
    }
}
