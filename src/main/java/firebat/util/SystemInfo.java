package firebat.util;

import firebat.log.Log;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;

public class SystemInfo
{
    private final static int MEGABYTE = 1048576;
    private static int availableProcessors;
    private static int maxMemory;

    private static final int TIME_CACHE_SIGAR = 1000;
    private static final Sigar   sigar; //DO NOT PUBLIC THIS OBJECT
    private static       long    timeCacheMem, timeCacheCpuPerc;
    private static       Mem     sigarMem;
    private static       CpuPerc sigarCpuPerc;

    static
    {
        sigar = new Sigar();
        Runtime runtime = Runtime.getRuntime();
        availableProcessors = runtime.availableProcessors();
        maxMemory = (int) (runtime.maxMemory() / MEGABYTE);
    }

    public static int availableProcessors ()
    {
        return availableProcessors;
    }

    private static void cacheSigarCpuPerc ()
    {
        synchronized (sigar)
        {
            long curTime = System.currentTimeMillis();
            if (curTime - timeCacheCpuPerc > TIME_CACHE_SIGAR || sigarCpuPerc == null)
            {
                timeCacheCpuPerc = curTime;
                try
                {
                    sigarCpuPerc = sigar.getCpuPerc();
                }
                catch (Exception e)
                {
                    sigarCpuPerc = null;
                    Log.exception(e);
                }
            }
        }
    }

    private static void cacheSigarMem ()
    {
        synchronized (sigar)
        {
            long curTime = System.currentTimeMillis();
            if (curTime - timeCacheMem > TIME_CACHE_SIGAR || sigarMem == null)
            {
                timeCacheMem = curTime;
                try
                {
                    sigarMem = sigar.getMem();
                }
                catch (Exception e)
                {
                    sigarMem = null;
                    Log.exception(e);
                }
            }
        }
    }

    public static int getCpuUsed ()
    {
        cacheSigarCpuPerc();
        if (sigarCpuPerc != null)
            return (int) Math.floor(sigarCpuPerc.getCombined() * 100);
        return -1;
    }

    public static int getMemoryUsedPercent ()
    {
        cacheSigarMem();
        if (sigarMem != null)
            return (int) Math.floor(sigarMem.getUsedPercent());
        return -1;
    }

    public static int getJvmMemoryUsed ()
    {
        return (int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / MEGABYTE);
    }

    public static int getJvmMaxMemory ()
    {
        //return (int) (Runtime.getRuntime().maxMemory() / MEGABYTE);
        return maxMemory;
    }

    public static int getJvmTotalMemory ()
    {
        return (int) (Runtime.getRuntime().totalMemory() / MEGABYTE);
    }

    public static int getJvmFreeMemory ()
    {
        return (int) (Runtime.getRuntime().freeMemory() / MEGABYTE);
    }
}
