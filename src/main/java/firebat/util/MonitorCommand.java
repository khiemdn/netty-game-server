package firebat.util;

import java.util.HashMap;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by CPU10399-local on 1/7/2016.
 */
public abstract class MonitorCommand<K>
{
    private final static char SEPARATOR_CMD  = ';';
    private final static char SEPARATOR_INFO = ',';

    public abstract boolean read (K id, int size);

    public abstract boolean handle (K id, long time);

    public abstract boolean write (K id, int size);

    public abstract HashMap<K, Info> snapshot (boolean isReset);

    public CharSequence toLogSnapshot (boolean isReset)
    {
        StringBuilder s = new StringBuilder();
        for (Info i : snapshot(isReset).values())
        {
            s.append(i.id).append(SEPARATOR_INFO)
             .append(i.numRead).append(SEPARATOR_INFO)
             .append(i.sizeRead).append(SEPARATOR_INFO)
             .append(i.numHandle).append(SEPARATOR_INFO)
             .append(i.timeHandle).append(SEPARATOR_INFO)
             .append(i.numWrite).append(SEPARATOR_INFO)
             .append(i.sizeWrite).append(SEPARATOR_CMD);
        }
        return s;
    }

    public static class Info<K>
    {
        public final K id;
        public final long numRead;
        public final long sizeRead;
        public final long numWrite;
        public final long sizeWrite;
        public final long numHandle;
        public final long timeHandle;

        public Info (K id, long numRead, long sizeRead, long numWrite, long sizeWrite, long numHandle, long timeHandle)
        {
            this.id = id;
            this.numRead = numRead;
            this.sizeRead = sizeRead;
            this.numWrite = numWrite;
            this.sizeWrite = sizeWrite;
            this.numHandle = numHandle;
            this.timeHandle = timeHandle;
        }

        public boolean inUsed ()
        {
            return numRead != 0 || numHandle != 0 || numWrite != 0;
        }
    }

    public static class InfoAdder<K>
    {
        K id;
        LongAdder numRead    = new LongAdder();
        LongAdder sizeRead   = new LongAdder();
        LongAdder numWrite   = new LongAdder();
        LongAdder sizeWrite  = new LongAdder();
        LongAdder numHandle  = new LongAdder();
        LongAdder timeHandle = new LongAdder();

        public InfoAdder (K id)
        {
            this.id = id;
        }

        public K getId ()
        {
            return id;
        }

        public Info<K> snapshot (boolean isReset)
        {
            Info<K> info = new Info<>(id, numRead.sum(), sizeRead.sum(), numWrite.sum(), sizeWrite.sum(), numHandle.sum(), timeHandle.sum());
            if (isReset)
            {
                numRead.add(-info.numRead);
                sizeRead.add(-info.sizeRead);
                numWrite.add(-info.numWrite);
                sizeWrite.add(-info.sizeWrite);
                numHandle.add(-info.numHandle);
                timeHandle.add(-info.timeHandle);
            }
            return info;
        }
    }
}
