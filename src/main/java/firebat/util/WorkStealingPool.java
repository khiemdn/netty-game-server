package firebat.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by CPU10399-local on 1/6/2016.
 */
public class WorkStealingPool
{
    private final static ExecutorService executor = Executors.newWorkStealingPool();
    private final static LongAdder count = new LongAdder();

    public static void shutdown ()
    {
        executor.shutdown();
    }

    public static void submit (Task task)
    {
        count.increment();
        task.isCount = true;
        executor.submit(task);
    }

    public static int queueSize ()
    {
        return count.intValue();
    }

    public static abstract class Task implements Runnable
    {
        private boolean isCount = false;

        public abstract void handle();

        @Override
        public final void run ()
        {
            if (isCount)
            {
                count.decrement();
                isCount = false;
            }
            handle();
        }
    }
}
