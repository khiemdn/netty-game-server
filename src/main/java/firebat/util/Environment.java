package firebat.util;

public enum Environment
{
    LIVE_GLOBAL(true, true),
    LIVE_VIETNAM(true, false),
    TEST_GLOBAL(false, true),
    TEST_VIETNAM(false, false),
    DEV(false, true);

    private boolean isServerLive;
    private boolean isServerGlobal;

    Environment(boolean isServerLive, boolean isServerGlobal) {
        this.isServerLive = isServerLive;
        this.isServerGlobal = isServerGlobal;
    }

    public boolean isServerLive ()
    {
        return isServerLive;
    }

    public boolean isServerGlobal ()
    {
        return isServerGlobal;
    }
}
