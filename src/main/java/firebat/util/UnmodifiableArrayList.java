package firebat.util;

import java.util.*;

/**
 * Created by thuanvt on 1/16/2015.
 */
public class UnmodifiableArrayList<E> extends ArrayList<E>
{
    public UnmodifiableArrayList (Collection<? extends E> c)
    {
        super(c);
    }

    @Override
    public void trimToSize ()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void ensureCapacity (int minCapacity)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public E set (int index, E element)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add (E e)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add (int index, E element)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove (Object o)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public E remove (int index)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear ()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll (Collection<? extends E> c)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll (int index, Collection<? extends E> c)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange (int fromIndex, int toIndex)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll (Collection<?> c)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll (Collection<?> c)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<E> subList (int fromIndex, int toIndex)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<E> listIterator (int index)
    {
        return new UnmodifiableListIterator<>(super.listIterator(index));
    }

    @Override
    public ListIterator<E> listIterator ()
    {
        return new UnmodifiableListIterator<>(super.listIterator());
    }

    @Override
    public Iterator<E> iterator ()
    {
        return new UnmodifiableIterator<>(super.iterator());
    }
}
