package firebat.util;

import firebat.log.Log;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by CPU01736-local on 3/10/2015.
 */
public class Random
{
    //1B:Using For Small Scale Of Array
    public static int getIndexRateSum(int[] rates)
    {
        if(rates == null || rates.length == 0)
            return -1;

        int sum = 0;
        int[] range = new int[rates.length];
        for (int i = 0; i < rates.length; i++) {
            sum += rates[i];
            range[i] = sum;
        }

        if(sum > 0)
        {
            ThreadLocalRandom random = ThreadLocalRandom.current();
            int rand = random.nextInt(0, sum + 1);
            for (int i = 0; i < rates.length; i++) {
                if(rand < range[i] && rates[i] > 0)
                    return i;
            }
        }

        return -1;
    }

    public static <E> Collection<E> collection (int numElement, Collection<E> source)
    {
        if (source == null || source.size() == 0 || numElement <= 0)
            return Collections.<E>emptyList();
        if (numElement >= source.size())
            return source;
        List<E> input = (source instanceof List) ? (List<E>) source : new ArrayList<E>(source);
        ArrayList<E> output = new ArrayList<>(numElement);
        for (int index : collection(numElement, 0, input.size()))
            output.add(input.get(index));
        return output;
    }

    //Returns array int which element is a unique pseudorandom int value between the specified origin (inclusive) and the specified bound (exclusive).
    public static Collection<Integer> collection (int numElement, int origin, int bound)
    {
        if (origin >= bound)
            throw new IllegalArgumentException("bound must be greater than origin");
        int delta = bound - origin;
        if (numElement > delta)
            throw new IllegalArgumentException("size must be equal or smaller than delta (bound - origin)");
        if (numElement == delta)
        {
            ArrayList<Integer> result = new ArrayList<>(numElement);
            for (int i = origin; i < bound; i++)
                result.add(i);
            return result;
        }

        HashSet<Integer> result = new HashSet<>(numElement);
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = bound - numElement; i < bound; i++)
            if (!result.add(random.nextInt(origin, i + 1)))
                result.add(i);

        return result;
    }


    public static String stringAlphaNum (int length)
    {
        final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        final int MAX = CHARS.length();

        StringBuilder sb = new StringBuilder(length);
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = length; i > 0; --i)
            sb.append(CHARS.charAt(random.nextInt(MAX)));
        return sb.toString();
    }
}
