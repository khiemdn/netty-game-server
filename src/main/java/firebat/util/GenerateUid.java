package firebat.util;

import firebat.log.Log;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by CPU10399-local on 5/28/2015.
 */
public class GenerateUid
{
    //Twitter snowflake: 41 bits : millisecond time, 10 bits: machine ID, 12 bits: sequence number. Sequence chỉ tăng khi trùng time và luôn bắt đầu từ 0
    //Fbs: 38 bits, 8 bits, 17 bits. Sequence khởi đầu ngẫu nhiên và tăng 1 sau mỗi lần sinh id
    private final static long NUM_BIT_TIME     = 38; //35 bit ~ 1 year, 36 bit ~ 2 year ...
    private final static long NUM_BIT_MACHINE  = 8;
    private final static long NUM_BIT_SEQUENCE = 63 - NUM_BIT_TIME - NUM_BIT_MACHINE;

    private final static long MASK_TIME     = -1L ^ (-1L << NUM_BIT_TIME);
    private final static long MASK_MACHINE  = -1L ^ (-1L << NUM_BIT_MACHINE);
    private final static long MASK_SEQUENCE = -1L ^ (-1L << NUM_BIT_SEQUENCE);

    private final static long SHIFT_TIME    = NUM_BIT_MACHINE + NUM_BIT_SEQUENCE;
    private final static long SHIFT_MACHINE = NUM_BIT_SEQUENCE;

    private final static long EPOCH_TIME = LocalDateTime.of(2015, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;

    private static long sequence, machine;

    static
    {
        try
        {
            if (NUM_BIT_TIME <= 0 || NUM_BIT_MACHINE <= 0 || NUM_BIT_SEQUENCE <= 0 || (NUM_BIT_TIME + NUM_BIT_MACHINE + NUM_BIT_SEQUENCE) != 63)
                throw new Exception("Wrong config in GenerateUid(" + NUM_BIT_TIME + "," + NUM_BIT_MACHINE + "," + NUM_BIT_SEQUENCE + ")");

            find_local_ip:
            for (NetworkInterface netIf : Collections.list(NetworkInterface.getNetworkInterfaces()))
            {
                for (InetAddress inetAddress : Collections.list(netIf.getInetAddresses()))
                {
                    if (inetAddress instanceof Inet4Address)
                    {
                        String host = inetAddress.getHostAddress();
                        if (host.startsWith("10."))
                        {
                            //machine = Hash.murmurHash3_x86_32(0, inetAddress.getAddress());
                            machine = inetAddress.getAddress()[3]; //Fbs: do server thường trong 1 VLAN và là IP V4 nên lấy số cuối là được -> 8bit
                            break find_local_ip;
                        }
                    }
                }
            }
            sequence = Math.abs(ThreadLocalRandom.current().nextInt());
            machine &= MASK_MACHINE;
            Log.info("GenerateUid", machine, NUM_BIT_TIME, NUM_BIT_MACHINE, NUM_BIT_SEQUENCE, EPOCH_TIME);
        }
        catch (Exception e)
        {
            Log.exception(e);
        }
    }

    public static synchronized long increaseSequence ()
    {
        return ++sequence;
    }

    public static long next ()
    {
        return next(increaseSequence());
    }

    public static long next (long sequence)
    {
        return (((System.currentTimeMillis() - EPOCH_TIME) & MASK_TIME) << SHIFT_TIME)
                | (machine << SHIFT_MACHINE)
                | (sequence & MASK_SEQUENCE);
    }

    public static void test ()
    {
        final int numTest = 10000;
        long[] results = new long[numTest];
        Log.debug("GenerateUid", "numTest", numTest);

        long time = System.nanoTime();
        for (int i = 0; i < numTest; i++)
            results[i] = next();
        time = System.nanoTime() - time;
        Log.debug("GenerateUid", "nanoTime", time);

        int numDuplicate = 0;
        HashSet<Long> setResult = new HashSet<>(numTest);
        for (int i = 0; i < numTest; i++)
            if (setResult.add(results[i]) == false)
                numDuplicate++;
        Log.debug("GenerateUid", "numDuplicate", numDuplicate);
    }
}
