package firebat.util;

import java.util.Iterator;

/**
 * Created by thuanvt on 1/16/2015.
 */
public class UnmodifiableIterator<E> implements Iterator<E>
{
    private final Iterator<? extends E> iterator;

    public UnmodifiableIterator (Iterator<? extends E> iterator)
    {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext ()
    {
        return iterator.hasNext();
    }

    @Override
    public E next ()
    {
        return iterator.next();
    }

    @Override
    public void remove ()
    {
        throw new UnsupportedOperationException();
    }
}
