package firebat.util;

import firebat.log.Log;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;

import java.net.*;
import java.util.*;

public class Address
{
    public final static List<InterfaceAddress> PUBLIC, PRIVATE;
    public final static String PRIVATE_HOST, BROADCAST_HOST, PUBLIC_HOST;
    public final static Set<String> SET_PRIVATE_VLAN;

    private String host;
    private int    port;

    static
    {
        ArrayList<InterfaceAddress> publicAddress = new ArrayList<>();
        ArrayList<InterfaceAddress> privateAddress = new ArrayList<>();
        HashSet<String> setPrivateVlan = new HashSet<>();
        setPrivateVlan.add("127.0.0.");
        
        String privateHost = null;
        String broadcastHost = null;
        String publicHost = null;
        String host;
        try
        {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements())
            {
                NetworkInterface networkInterface = interfaces.nextElement();
                if (networkInterface.isLoopback() || networkInterface.isUp() == false)
                    continue;
                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses())
                {
                    InetAddress address = interfaceAddress.getAddress();
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    if (address instanceof Inet4Address)
                    {
                        byte[] bytes = address.getAddress();
                        int byte0 = bytes[0] & 0xff;
                        int byte1 = bytes[1] & 0xff;
                        if ((byte0 == 192 && byte1 == 168) || (byte0 == 172 && (byte1 >= 16 || byte1 <= 31)))
                        {
                            privateAddress.add(interfaceAddress);
                        }
                        else if (byte0 == 10)
                        {
                            privateAddress.add(interfaceAddress);
                            host = address.getHostAddress();
                            setPrivateVlan.add(host.substring(0, host.lastIndexOf('.') + 1));

                            if (broadcast != null && privateHost == null)
                            {
                                privateHost = host;
                                broadcastHost = broadcast.getHostAddress();
                            }
                        }
                        else
                        {
                            if (publicHost == null)
                                publicHost = address.getHostAddress();
                            publicAddress.add(interfaceAddress);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.exception(e);
        }

        PRIVATE = Collections.unmodifiableList(privateAddress);
        PUBLIC = Collections.unmodifiableList(publicAddress);
        PRIVATE_HOST = privateHost;
        BROADCAST_HOST = broadcastHost;
        PUBLIC_HOST = publicHost;
        SET_PRIVATE_VLAN = Collections.unmodifiableSet(setPrivateVlan);
        Log.console("PRIVATE_HOST", PRIVATE_HOST, "BROADCAST_HOST", BROADCAST_HOST, "PUBLIC_HOST", PUBLIC_HOST);
    }

    public static boolean isPrivateVlan (InetAddress ia)
    {
        return isPrivateVlan(ia.getHostAddress());
    }

    public static boolean isPrivateVlan (String host)
    {
        return SET_PRIVATE_VLAN.contains(host.substring(0, host.lastIndexOf('.') + 1));
    }

    public static String getRemoteAddress (InetSocketAddress isa)
    {
        return isa.getAddress().getHostAddress();
    }

    public static String getRemoteIP (ChannelHandlerContext ctx)
    {
        if (ctx != null)
        {
            SocketAddress a = ctx.channel().remoteAddress();
            if (a instanceof InetSocketAddress)
                return getRemoteIP((InetSocketAddress) a);
        }
        return "";
    }

    public static String getRemoteIP (InetSocketAddress isa)
    {
        String fullAddress = getRemoteAddress(isa);
        // Address resolves to /x.x.x.x:zzzz we only want x.x.x.x
        if (fullAddress.startsWith("/"))
        {
            fullAddress = fullAddress.substring(1);
        }
        int i = fullAddress.indexOf(":");
        if (i != -1)
        {
            fullAddress = fullAddress.substring(0, i);
        }
        return fullAddress;
    }

    public static String get (ChannelHandlerContext ctx, HttpRequest request)
    {
        String r = getXForwardedFor(request);
        if (r.isEmpty())
        {
            r = ctx.channel().remoteAddress().toString();
            r = r.substring(1, r.indexOf(':'));
        }
        return r;
    }

    public static String getXForwardedFor (HttpRequest request)
    {
        String fw = request.headers().get("X-Forwarded-For");
        if (fw != null)
        {
            int pos = fw.indexOf(',');
            return (pos > 0) ? fw.substring(0, pos) : fw;
        }
        return "";
    }

    public static InetSocketAddress getInetSocketAddress (String inetHost, int inetPort)
    {
        return (inetHost == null || inetHost.isEmpty() || inetHost.length() <= 1)
                ? new InetSocketAddress(inetPort)
                : new InetSocketAddress(inetHost, inetPort);
    }

    public InetSocketAddress getInetSocketAddress ()
    {
        return getInetSocketAddress(host, port);
    }

    public String getHost ()
    {
        return host;
    }

    public void setHost (String host)
    {
        this.host = host;
    }

    public int getPort ()
    {
        return port;
    }

    public void setPort (int port)
    {
        this.port = port;
    }
}
