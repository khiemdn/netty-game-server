package firebat.util;

import java.util.Iterator;
import java.util.ListIterator;

/**
 * Created by thuanvt on 1/16/2015.
 */
public class UnmodifiableListIterator<E> implements ListIterator<E>
{
    private final ListIterator<? extends E> iterator;

    public UnmodifiableListIterator (ListIterator<? extends E> iterator)
    {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext ()
    {
        return iterator.hasNext();
    }

    @Override
    public E next ()
    {
        return iterator.next();
    }

    @Override
    public boolean hasPrevious ()
    {
        return iterator.hasPrevious();
    }

    @Override
    public E previous ()
    {
        return iterator.previous();
    }

    @Override
    public int nextIndex ()
    {
        return iterator.nextIndex();
    }

    @Override
    public int previousIndex ()
    {
        return iterator.previousIndex();
    }

    @Override
    public void remove ()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void set (E e)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add (E e)
    {
        throw new UnsupportedOperationException();
    }
}
