package firebat.util;

import firebat.log.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterOutputStream;

/**
 * Created by CPU10399-local on 9/23/2015.
 */
public class Misc
{
    public static int deleteFiles (String path)
    {
        try
        {
            Path p = Paths.get(path);
            if (Files.exists(p) == false)
                return 0;
            if (Files.isDirectory(p))
            {
                Files.walkFileTree(p, new SimpleFileVisitor<Path>()
                {
                    @Override
                    public FileVisitResult visitFile (Path file, BasicFileAttributes attrs) throws IOException
                    {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory (Path dir, IOException exc) throws IOException
                    {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }
                });
                return 1;
            }
            else
            {
                Files.delete(p);
                return 2;
            }
        }
        catch (Exception e)
        {
            Log.exception(e);
        }
        return -1;
    }

    public static boolean createFolder (String path)
    {
        try
        {
            Path p = Paths.get(path);
            if (Files.exists(p))
                return false;
            return Files.createDirectories(p) != null;
        }
        catch (Exception e)
        {
            Log.exception(e);
        }
        return false;
    }

    public static boolean createFile (String path, Object content)
    {
        try
        {
            Path p = Paths.get(path);
            if (Files.exists(p))
                return false;
            if (content instanceof byte[])
                return Files.write(p, (byte[]) content) != null;
            if (content instanceof String)
                return Files.write(p, ((String) content).getBytes("UTF-8")) != null;
        }
        catch (Exception e)
        {
            Log.exception(e);
        }
        return false;
    }

    public static byte[] compress (byte[] in)
    {
        if (in != null && in.length > 0)
        {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream(in.length >> 1);
                 DeflaterOutputStream dos = new DeflaterOutputStream(bos))
            {
                dos.write(in);
                dos.close();
                return bos.toByteArray();
            }
            catch (IOException e)
            {
                Log.exception(e);
            }
        }
        return null;
    }

    public static byte[] decompress (byte[] in)
    {
        if (in != null && in.length > 0)
        {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream(in.length << 1);
                 InflaterOutputStream dos = new InflaterOutputStream(bos))
            {
                dos.write(in);
                dos.close();
                return bos.toByteArray();
            }
            catch (IOException e)
            {
                Log.exception(e);
            }
        }
        return null;
    }

    public static byte[] gzipCompress (byte[] in)
    {
        if (in != null && in.length > 0)
        {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream(in.length >> 1);
                 GZIPOutputStream dos = new GZIPOutputStream(bos))
            {
                dos.write(in);
                dos.close();
                return bos.toByteArray();
            }
            catch (IOException e)
            {
                Log.exception(e);
            }
        }
        return null;
    }

    public static byte[] gzipDecompress (byte[] in)
    {
        if (in != null && in.length > 0)
        {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream(in.length << 1);
                 ByteArrayInputStream bis = new ByteArrayInputStream(in);
                 GZIPInputStream dos = new GZIPInputStream(bis))
            {
                byte[] buf = new byte[8192];
                int r;
                while ((r = dos.read(buf)) > 0)
                {
                    bos.write(buf, 0, r);
                }

                return bos.toByteArray();
            }
            catch (IOException e)
            {
                Log.exception(e);
            }
        }
        return null;
    }

    public static byte[] gzipDecompress (InputStream in, int size)
    {
        if (in != null && size > 0)
        {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream(size << 1);
                 GZIPInputStream dos = new GZIPInputStream(in))
            {
                byte[] buf = new byte[8192];
                int r;
                while ((r = dos.read(buf)) > 0)
                {
                    bos.write(buf, 0, r);
                }

                return bos.toByteArray();
            }
            catch (IOException e)
            {
                Log.exception(e);
            }
        }
        return null;
    }
}
