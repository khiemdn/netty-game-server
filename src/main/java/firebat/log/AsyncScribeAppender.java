package firebat.log;

import org.apache.log4j.AsyncAppender;

public class AsyncScribeAppender extends AsyncAppender
{
    private String scribeHost;
    private int scribePort;
    private String folderBackup;
    private long timeToWaitBeforeRetry;
   
    public String getScribeHost ()
    {
        return scribeHost;
    }

    public void setScribeHost (String scribeHost)
    {
        this.scribeHost = scribeHost;
    }

    public int getScribePort ()
    {
        return scribePort;
    }

    public void setScribePort (int scribePort)
    {
        this.scribePort = scribePort;
    }
    
    public String getFolderBackup ()
    {
        return folderBackup;
    }

    public void setFolderBackup (String folderBackup)
    {
        this.folderBackup = folderBackup;
    }
    
     public long getTimeToWaitBeforeRetry () 
    {
        return timeToWaitBeforeRetry;
    }

    public void setTimeToWaitBeforeRetry (long timeToWaitBeforeRetry) 
    {
        this.timeToWaitBeforeRetry = timeToWaitBeforeRetry;
    }

    @Override
    public void activateOptions ()
    {
        super.activateOptions();
        synchronized (this)
        {
            ScribeAppender scribeAppender = new ScribeAppender();
            scribeAppender.setLayout(getLayout());
            scribeAppender.setScribeHost(getScribeHost());
            scribeAppender.setScribePort(getScribePort());            
            scribeAppender.setTimeToWaitBeforeRetry(getTimeToWaitBeforeRetry());            
            scribeAppender.setFolderBackup(getFolderBackup());            
            scribeAppender.activateOptions();
            
            addAppender(scribeAppender);
        }
    }

    @Override
    public boolean requiresLayout ()
    {
        return true;
    }

}
