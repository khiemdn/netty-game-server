package firebat.log;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.LongAdder;

public class Log
{
    public final static char SEPARATOR_IFRS        = ',';
    public final static char SEPARATOR_GAME        = '\t';
    public final static char SEPARATOR_ACTION_NUM  = ':';
    public final static char SEPARATOR_ACTION_ITEM = ',';

    //    private final static SimpleDateFormat  formatterIfrs = new SimpleDateFormat("yyyyMMdd"); //JDK <= 7
    private final static DateTimeFormatter formatterIfrs = DateTimeFormatter.ofPattern("yyyyMMdd"); //JDK >= 8

    private static LongAdder numException = new LongAdder();

    private static boolean useDebug               = true;
    private static boolean useConsoleForException = false;

    private static ThreadLocal<StringBuilder> cacheStringBuilder = new ThreadLocal<StringBuilder>()
    {
        @Override
        protected StringBuilder initialValue ()
        {
            return new StringBuilder(512);
        }

        @Override
        public StringBuilder get ()
        {
            StringBuilder b = super.get();
            b.setLength(0);
            return b;
        }
    };

    public static Logger loggerConsole,
            loggerInfo,
            loggerIfrs,
            loggerRegister,
            loggerRoleCreated,
            loggerLogin,
            loggerLogout,
            loggerPaying,
            loggerSpentCoin,
            loggerSocial,
            loggerLevelUp,
            loggerTracking,
            loggerSnapshot,
            loggerChat,
            loggerAction,
            loggerKeyItem,
            loggerSystem,

    //Log c?, c?n xem l?i
    loggerBackup,
            loggerGM;

    public synchronized static void start (String filePath, boolean isDebug) throws Exception
    {
        useDebug = isDebug;

        PropertyConfigurator.configure(filePath);

        Path path = Paths.get("./log");
        if (Files.exists(path) == false)
        {
            Files.createDirectories(path);
        }

        loggerConsole = getLogger("CONSOLE");
        loggerInfo = getLogger("INFO");
        loggerIfrs = getLogger("IFRS");
        loggerRegister = getLogger("REGISTER");
        loggerRoleCreated = getLogger("ROLE_CREATED");
        loggerLogin = getLogger("LOGIN");
        loggerLogout = getLogger("LOGOUT");
        loggerPaying = getLogger("PAYING");
        loggerSpentCoin = getLogger("SPENT_COIN");
        loggerSocial = getLogger("SOCIAL");
        loggerLevelUp = getLogger("LEVEL_UP");
        loggerTracking = getLogger("TRACKING");
        loggerChat = getLogger("CHAT");
        loggerAction = getLogger("ACTION");
        loggerSystem = getLogger("SYSTEM");
        loggerBackup = getLogger("BACKUP");
        loggerGM = getLogger("GM");
        loggerSnapshot = getLogger("SNAPSHOT");
        loggerKeyItem = getLogger("KEY_ITEM");

        Log.console("Use " + filePath);
    }

    public synchronized static void stop ()
    {
        LogManager.shutdown();
        ScribeAppender.closeBackupFile();

        loggerConsole = null;
        loggerInfo = null;
        loggerIfrs = null;
        loggerRegister = null;
        loggerRoleCreated = null;
        loggerLogin = null;
        loggerLogout = null;
        loggerPaying = null;
        loggerSpentCoin = null;
        loggerSocial = null;
        loggerLevelUp = null;
        loggerTracking = null;
        loggerChat = null;
        loggerAction = null;
        loggerSystem = null;
        loggerBackup = null;
        loggerGM = null;
        loggerSnapshot = null;
        loggerKeyItem = null;
    }

    public static void forceUseConsoleForException (boolean value)
    {
        useConsoleForException = value;
    }

    private static Logger getLogger (String name) throws Exception
    {
        Logger logger = LogManager.exists(name);
        if (logger == null)
            throw new Exception("Not found logger " + name + " in properties");
        return logger;
    }

    public static StringBuilder getCharSequence (char separator, Object... acs)
    {
        return getCharSequence(cacheStringBuilder.get(), separator, acs);
    }

    public static StringBuilder getCharSequence (StringBuilder sb, char separator, Object... acs)
    {
        for (Object o : acs)
        {
            sb.append(o);
            if (separator > 0)
            {
                sb.append(separator);
            }
        }
        return sb;
    }

    public static void debug (Object... acs)
    {
        if (useDebug)
        {
            console(acs);
        }
    }

    public static void console (Object... acs)
    {
        StringBuilder msg = getCharSequence(SEPARATOR_GAME, acs);
        if (loggerConsole == null)
        {
            System.out.println(msg);
        }
        else
        {
            loggerConsole.info(msg);
        }
    }

    public static void info (Object... acs)
    {
        StringBuilder msg = getCharSequence(SEPARATOR_GAME, acs);
        if (loggerInfo == null)
        {
            System.out.println(msg);
        }
        else
        {
            loggerInfo.info(msg);
        }
    }

    public static void infoToCategory (String category, Object... acs)
    {
        StringBuilder msg = getCharSequence(SEPARATOR_GAME, acs);
        if (loggerInfo == null)
        {
            System.out.println(msg);
        }
        else
        {
            loggerInfo.infoToCategory(category, msg);
        }
    }

    public static StringBuilder throwableToString (Throwable cause, StringBuilder sb)
    {
        if (sb == null)
            sb = cacheStringBuilder.get();
        sb.append(cause.toString()).append('\n');
        for (StackTraceElement ste : cause.getStackTrace())
        {
            sb.append(ste.toString()).append('\n');
        }
        return sb;
    }

    public static void exception (Throwable cause, Object... acs)
    {
        numException.increment();
        if (loggerInfo == null)
        {
            cause.printStackTrace();
        }
        else
        {
            StringBuilder sb = cacheStringBuilder.get();
            getCharSequence(sb, SEPARATOR_GAME, acs);
            throwableToString(cause, sb);
            if (useConsoleForException)
                loggerConsole.info(sb);
            else
                loggerInfo.info(sb);
        }
    }

    public static void ifrs (
            Object account,
            long chargeCoinExist,
            long bonusCoinExist,
            long unixTime,
            CharSequence serverId,
            long chargeCoinExchange,
            long bonusCoinExchange,
            Object itemId, //CharSequence itemId,
            Object actionId,
            long unitPrice,
            long grossRevenue,
            long netRevenue,
            boolean isMobile, //CharSequence userSource,
            Object transactionId
                            )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(account).append(SEPARATOR_IFRS)
                                             .append(chargeCoinExist).append(SEPARATOR_IFRS)
                                             .append(bonusCoinExist).append(SEPARATOR_IFRS)
                                             .append(unixTime).append(SEPARATOR_IFRS)
                                             .append(serverId).append(SEPARATOR_IFRS)
                                             .append(chargeCoinExchange).append(SEPARATOR_IFRS)
                                             .append(bonusCoinExchange).append(SEPARATOR_IFRS)
                                             .append(itemId).append(SEPARATOR_IFRS)
                                             .append(actionId).append(SEPARATOR_IFRS)
                                             .append(unitPrice).append(SEPARATOR_IFRS)
                                             .append(grossRevenue).append(SEPARATOR_IFRS)
                                             .append(netRevenue).append(SEPARATOR_IFRS)
                                             .append(isMobile ? "mobile" : "web").append(SEPARATOR_IFRS)
                                             .append(transactionId).append(SEPARATOR_IFRS);

//        loggerIfrs.infoToCategory("IFRS_" + formatterIfrs.format(new Date()), sb); //JDK <= 7
        loggerIfrs.infoToCategory("IFRS_" + LocalDate.now().format(formatterIfrs), sb); //JDK >= 8
    }


    public static void register (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vGameClientVersion,
            CharSequence vOSPlatform,
            CharSequence vOSVersion,
            CharSequence vDeviceID,
            CharSequence vDeviceName,
            CharSequence vConnectionType,
            CharSequence vDownloadSource,
            CharSequence vThirdPartySource,
            long iResult,
            Object... ao
                                )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vGameClientVersion).append(SEPARATOR_GAME)
                                             .append(vOSPlatform).append(SEPARATOR_GAME)
                                             .append(vOSVersion).append(SEPARATOR_GAME)
                                             .append(vDeviceID).append(SEPARATOR_GAME)
                                             .append(vDeviceName).append(SEPARATOR_GAME)
                                             .append(vConnectionType).append(SEPARATOR_GAME)
                                             .append(vDownloadSource).append(SEPARATOR_GAME)
                                             .append(vThirdPartySource).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerRegister.info(sb);
    }

    public static void roleCreated (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iClass,
            int iGender,
            int iResult
                                   )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iClass).append(SEPARATOR_GAME)
                                             .append(iGender).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME);

        loggerRoleCreated.info(sb);
    }

    public static void login (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            CharSequence vGameClientVersion,
            CharSequence vOSPlatform,
            CharSequence vOSVersion,
            CharSequence vDeviceID,
            CharSequence vDeviceName,
            CharSequence vConnectionType,
            int iLevel,
            long iExp,
            long iCoinBalance,
            long iCashCoinBalance,
            long iBonusCoinBalance,
            CharSequence vSessionID,
            int iResult,
            Object... ao
                             )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(vGameClientVersion).append(SEPARATOR_GAME)
                                             .append(vOSPlatform).append(SEPARATOR_GAME)
                                             .append(vOSVersion).append(SEPARATOR_GAME)
                                             .append(vDeviceID).append(SEPARATOR_GAME)
                                             .append(vDeviceName).append(SEPARATOR_GAME)
                                             .append(vConnectionType).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(iExp).append(SEPARATOR_GAME)
                                             .append(iCoinBalance).append(SEPARATOR_GAME)
                                             .append(iCashCoinBalance).append(SEPARATOR_GAME)
                                             .append(iBonusCoinBalance).append(SEPARATOR_GAME)
                                             .append(vSessionID).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerLogin.info(sb);
    }

    public static void logout (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            CharSequence vGameClientVersion,
            CharSequence vOSPlatform,
            CharSequence vOSVersion,
            CharSequence vDeviceID,
            CharSequence vDeviceName,
            CharSequence vConnectionType,
            int iLevel,
            long iExp,
            long iCoinBalance,
            long iCashCoinBalance,
            long iBonusCoinBalance,
            CharSequence vSessionID,
            long iTotalOnlineSecond,
            int iResult,
            Object... ao
                              )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(vGameClientVersion).append(SEPARATOR_GAME)
                                             .append(vOSPlatform).append(SEPARATOR_GAME)
                                             .append(vOSVersion).append(SEPARATOR_GAME)
                                             .append(vDeviceID).append(SEPARATOR_GAME)
                                             .append(vDeviceName).append(SEPARATOR_GAME)
                                             .append(vConnectionType).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(iExp).append(SEPARATOR_GAME)
                                             .append(iCoinBalance).append(SEPARATOR_GAME)
                                             .append(iCashCoinBalance).append(SEPARATOR_GAME)
                                             .append(iBonusCoinBalance).append(SEPARATOR_GAME)
                                             .append(vSessionID).append(SEPARATOR_GAME)
                                             .append(iTotalOnlineSecond).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME);

        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerLogout.info(sb);
    }

    public static void paying (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            CharSequence vPaymentGateway,
            CharSequence vPayingTypeID,
            Object vTransactionID,
            CharSequence vIPPaying,
            long iGrossRevenue_User,
            long iGrossRevenue,
            long iTopupCoin,
            long iCashCoin,
            long iBonusCoin,
            long iTopupCoinAfter,
            long iCashCoinAfter,
            long iBonusCoinAfter,
            CharSequence vDescription,
            int iResult,
            int iLevel,
            boolean iIsFirstPaying,
            CharSequence OSPlatform
                              )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(vPaymentGateway).append(SEPARATOR_GAME)
                                             .append(vPayingTypeID).append(SEPARATOR_GAME)
                                             .append(vTransactionID).append(SEPARATOR_GAME)
                                             .append(vIPPaying).append(SEPARATOR_GAME)
                                             .append(iGrossRevenue_User).append(SEPARATOR_GAME)
                                             .append(iGrossRevenue).append(SEPARATOR_GAME)
                                             .append(iTopupCoin).append(SEPARATOR_GAME)
                                             .append(iCashCoin).append(SEPARATOR_GAME)
                                             .append(iBonusCoin).append(SEPARATOR_GAME)
                                             .append(iTopupCoinAfter).append(SEPARATOR_GAME)
                                             .append(iCashCoinAfter).append(SEPARATOR_GAME)
                                             .append(iBonusCoinAfter).append(SEPARATOR_GAME)
                                             .append(vDescription).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(iIsFirstPaying ? '1' : '0').append(SEPARATOR_GAME)
                                             .append(OSPlatform).append(SEPARATOR_GAME);
        loggerPaying.info(sb);
    }

    public static void spentCoin (
            CharSequence vActionName,
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iLevel,
            CharSequence vTransactionID,
            CharSequence vIPSpent,
            boolean isIbShop, //int iShopTypeID,
            Object vItemID,//CharSequence vItemID,
            Object vItemName,
            long iItemPrice,
            long iQuantity,
            long iSpentCoin,
            long iCashCoinSpent,
            long iBonusCoinSpent,
            long iCoinAfter,
            long iCashCoinAfter,
            long iBonusCoinAfter,
            CharSequence vDescription,
            int iResult,
            CharSequence OSPlatform
                                 )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vActionName).append(SEPARATOR_GAME)
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(vTransactionID).append(SEPARATOR_GAME)
                                             .append(vIPSpent).append(SEPARATOR_GAME)
                                             .append(isIbShop ? '1' : '2').append(SEPARATOR_GAME)
                                             .append(vItemID).append(SEPARATOR_GAME)
                                             .append(vItemName).append(SEPARATOR_GAME)
                                             .append(iItemPrice).append(SEPARATOR_GAME)
                                             .append(iQuantity).append(SEPARATOR_GAME)
                                             .append(iSpentCoin).append(SEPARATOR_GAME)
                                             .append(iCashCoinSpent).append(SEPARATOR_GAME)
                                             .append(iBonusCoinSpent).append(SEPARATOR_GAME)
                                             .append(iCoinAfter).append(SEPARATOR_GAME)
                                             .append(iCashCoinAfter).append(SEPARATOR_GAME)
                                             .append(iBonusCoinAfter).append(SEPARATOR_GAME)
                                             .append(vDescription).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME)
                                             .append(OSPlatform).append(SEPARATOR_GAME);

        loggerSpentCoin.info(sb);
    }

    public static void social (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            Object vSocialName,
            Object vSocialID,
            Object vSocialAccount,
            CharSequence vSocialDisplayName,
            int iGender,
            CharSequence vPhoneNumber,
            CharSequence vBirthday,
            Object... ao
                              )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(vSocialName).append(SEPARATOR_GAME)
                                             .append(vSocialID).append(SEPARATOR_GAME)
                                             .append(vSocialAccount).append(SEPARATOR_GAME)
                                             .append(vSocialDisplayName).append(SEPARATOR_GAME)
                                             .append(iGender).append(SEPARATOR_GAME)
                                             .append(vPhoneNumber).append(SEPARATOR_GAME)
                                             .append(vBirthday).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerSocial.info(sb);
    }

    public static void levelUp (
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iOldLevel,
            int iNewLevel,
            long iExp,
            long iPlayingTime,
            CharSequence vDescription,
            Object... ao
                               )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iOldLevel).append(SEPARATOR_GAME)
                                             .append(iNewLevel).append(SEPARATOR_GAME)
                                             .append(iExp).append(SEPARATOR_GAME)
                                             .append(iPlayingTime).append(SEPARATOR_GAME)
                                             .append(vDescription).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerLevelUp.info(sb);
    }

    private static void tracking (
            CharSequence vActionName,
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iStepID,
            long iPlayingTime,
            int iResult
                                 )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vActionName).append(SEPARATOR_GAME)
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iStepID).append(SEPARATOR_GAME)
                                             .append(iPlayingTime).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME);

        loggerTracking.info(sb);
    }

    public static void chat (
            int iChatType,
            CharSequence vRoomName,
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iLevel,
            CharSequence vMsg
                            )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(iChatType).append(SEPARATOR_GAME)
                                             .append(vRoomName).append(SEPARATOR_GAME)
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(vMsg).append(SEPARATOR_GAME);

        loggerChat.info(sb);
    }

    public static void action (
            CharSequence vActionName,
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iLevel,
            long iPlayingTime,
            int iSuccessPercent,
            Object vTransactionID,
            CharSequence vItemLostList,
            CharSequence vItemReceivedList,
            Object... ao
                              )
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vActionName).append(SEPARATOR_GAME)
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(iPlayingTime).append(SEPARATOR_GAME)
                                             .append(iSuccessPercent).append(SEPARATOR_GAME)
                                             .append(vTransactionID).append(SEPARATOR_GAME)
                                             .append(vItemLostList).append(SEPARATOR_GAME)
                                             .append(vItemReceivedList).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerAction.infoToCategory("ACTION_" + vActionName, sb);
    }

    public static void system (CharSequence vServerID, CharSequence type, Object... ao)
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(type).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerSystem.info(sb);
    }

    public static void gm (CharSequence adminId, Object requestAddress, CharSequence command, Object... ao)
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(adminId).append(SEPARATOR_GAME)
                                             .append(requestAddress).append(SEPARATOR_GAME)
                                             .append(command).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerGM.info(sb);
    }

    public static void backup (Object vUserID,
                               CharSequence vServerID,
                               CharSequence vRoleID,
                               CharSequence vRoleName,
                               Object... ao)
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerBackup.info(sb);
    }

    public static void snapshot (
            CharSequence vSnapshotName,
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            CharSequence vRoleName,
            int iLevel,
            CharSequence vSnapshot_Info,
            Object... ao)
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vSnapshotName).append(SEPARATOR_GAME)
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(vRoleName).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(vSnapshot_Info).append(SEPARATOR_GAME);
        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerSnapshot.info(sb);
    }

    public static void trackingKeyItem (
            Object itemId,
            CharSequence vTransactionID,
            CharSequence vActionName,
            CharSequence vActionNameDetail,
            Object vUserID,
            CharSequence vServerID,
            CharSequence vRoleID,
            int iLevel,
            boolean isReceive, //true: nhận, false: mất đi
            long iQuantity,
            long iQuantityAfter,
            int iResult,
            Object... ao)
    {
        StringBuilder sb = cacheStringBuilder.get()
                                             .append(vTransactionID).append(SEPARATOR_GAME)
                                             .append(vActionName).append(SEPARATOR_GAME)
                                             .append(vActionNameDetail).append(SEPARATOR_GAME)
                                             .append(vUserID).append(SEPARATOR_GAME)
                                             .append(vServerID).append(SEPARATOR_GAME)
                                             .append(vRoleID).append(SEPARATOR_GAME)
                                             .append(iLevel).append(SEPARATOR_GAME)
                                             .append(isReceive ? "in" : "out").append(SEPARATOR_GAME)
                                             .append(iQuantity).append(SEPARATOR_GAME)
                                             .append(iQuantityAfter).append(SEPARATOR_GAME)
                                             .append(iResult).append(SEPARATOR_GAME)
                                             .append(itemId).append(SEPARATOR_GAME);

        getCharSequence(sb, SEPARATOR_GAME, ao);

        loggerKeyItem.infoToCategory("KEYITEM_" + itemId, sb);
    }

    public static int getNumException ()
    {
        return numException.intValue();
    }
}
