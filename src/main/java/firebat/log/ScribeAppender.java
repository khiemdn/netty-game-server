package firebat.log;

import firebat.util.Time;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import scribe.thrift.LogEntry;
import scribe.thrift.ResultCode;
import scribe.thrift.scribe.Client;

import java.io.BufferedWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ScribeAppender extends AppenderSkeleton
{
    private String scribeHost;
    private int scribePort;
    private long timeToWaitBeforeRetry = 5_000;
    private String folderBackup;
    private int maxFileSize = 1_000_000;    

    // NOTE: logEntries, client, and transport are all protected by a lock on 'this.'
    // The Scribe interface for sending log messages accepts a list.  This list is created
    // once and cleared and appended when new logs are created.  The list is always size 1.
    private List<LogEntry> logEntries;

    private Client client;
    private TFramedTransport transport;

    private boolean valid = false;
    private long retryTime = 0;

    private static final ConcurrentHashMap<String, FileAppender> mapBackupFile = new ConcurrentHashMap<>();
    private final static String dateFilename = "yyMMdd_HHmmss_SSS";

    public String getScribeHost ()
    {
        return scribeHost;
    }

    public void setScribeHost (String scribeHost)
    {
        this.scribeHost = scribeHost;
    }

    public int getScribePort ()
    {
        return scribePort;
    }

    public void setScribePort (int scribePort)
    {
        this.scribePort = scribePort;
    }

    public String getFolderBackup ()
    {
        return folderBackup;
    }

    public void setFolderBackup (String folderBackup)
    {
        this.folderBackup = folderBackup;
    }
    
    public int getMaxFileSize ()
    {
        return maxFileSize;
    }

    public void setMaxFileSize (int maxFileSize)
    {
        this.maxFileSize = maxFileSize;
    }

    public long getTimeToWaitBeforeRetry ()
    {
        return timeToWaitBeforeRetry;
    }

    public void setTimeToWaitBeforeRetry (long timeToWaitBeforeRetry)
    {
        this.timeToWaitBeforeRetry = timeToWaitBeforeRetry;
    }

    public static void closeBackupFile ()
    {
        for (String k : mapBackupFile.keySet())
        {
            FileAppender fa = mapBackupFile.remove(k);
            if (fa != null)
            {
                fa.close();
            }
        }
    }

    @Override
    public void activateOptions ()
    {
        connect(true);
    }

    private boolean connect (boolean printStackTrace)
    {
        synchronized (this)
        {
            if ((valid == false) && (System.currentTimeMillis() > retryTime))
            {
                Socket socket = null;
                try
                {
                    logEntries = new ArrayList<>(1);
                    socket = new Socket(scribeHost, scribePort);
                    TSocket sock = new TSocket(socket);
                    transport = new TFramedTransport(sock);
                    TBinaryProtocol protocol = new TBinaryProtocol(transport, false, false);
                    client = new Client(protocol, protocol);

                    valid = true;
                }
                catch (Exception e)
                {
                    if (socket != null)
                    {
                        try
                        {
                            socket.close();
                        }
                        catch (Exception e1)
                        {
                        }
                    }
                    handleConnectionFailure();
                    if (printStackTrace)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
        return valid;
    }

    private void handleConnectionFailure ()
    {
        close();
        client = null;
        transport = null;
        valid = false;
        retryTime = System.currentTimeMillis() + timeToWaitBeforeRetry;
    }

    @Override
    public void append (LoggingEvent event)
    {
        synchronized (this)
        {
            String message = null;
            String loggerName = event.getLoggerName();

            if (valid == false)
            {
                if (connect(false))
                {
                    FileAppender fa = mapBackupFile.remove(loggerName);
                    if (fa != null)
                    {
                        fa.close();
                    }
                }
            }

            try
            {
                message = layout.format(event);
                if (valid)
                {
                    LogEntry entry = new LogEntry(loggerName, message);

                    logEntries.add(entry);
                    if (client.Log(logEntries) == ResultCode.OK)
                    {
                        message = null;
                    }
                    else
                    {
                        handleConnectionFailure();
                    }
                }
            }
            catch (TException te)
            {
                handleConnectionFailure();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                logEntries.clear();
            }

            if (message != null)
            {
                try
                {
                    FileAppender fa = mapBackupFile.get(loggerName);
                    if (fa == null)
                    {
                        fa = new FileAppender(loggerName);
                        mapBackupFile.put(loggerName, fa);
                    }
                    fa.append(message);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void close ()
    {
        if (transport != null)
        {
            transport.close();
        }
    }

    @Override
    public boolean requiresLayout ()
    {
        return true;
    }

    private class FileAppender
    {
        private final String loggerName;
        private BufferedWriter writer;
        private int curSize = 0;

        private FileAppender (String loggerName)
        {
            this.loggerName = loggerName;
        }

        private synchronized void append (String msg) throws Exception
        {
            curSize += msg.length();
            if (curSize >= maxFileSize)
            {
                close();
            }
            if (writer == null)
            {
                String folder = folderBackup + "/" + loggerName;
                Path path = Paths.get(folder);
                if (Files.exists(path) == false)
                {
                    Files.createDirectories(path);
                }
                writer = Files.newBufferedWriter(Paths.get(folder + "/log-" + Time.currentDateString(dateFilename) + ".txt"),
                        StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
            writer.write(msg);
            writer.newLine();
        }

        private void close ()
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                }
                writer = null;
                curSize = 0;
            }
        }
    }
}
