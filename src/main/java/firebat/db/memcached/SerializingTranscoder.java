/**
 * Copyright (C) 2006-2009 Dustin Sallings
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALING
 * IN THE SOFTWARE.
 */

package firebat.db.memcached;

import java.util.Date;

import firebat.log.Log;
import net.spy.memcached.CachedData;
import net.spy.memcached.transcoders.*;
import net.spy.memcached.util.StringUtils;

/**
 * Transcoder that serializes and compresses objects.
 */
//public class SerializingTranscoder extends net.spy.memcached.transcoders.BaseSerializingTranscoder implements
 public class SerializingTranscoder extends BaseSerializingTranscoder implements
        Transcoder<Object> {

    // General flags    
    private final int COMPRESSED;

    // Special flags for specially handled types.
    private final int SPECIAL_MASK = 0xff00;    
    private final int SPECIAL_BYTEARRAY = (8 << 8);

    private final TranscoderUtils tu = new TranscoderUtils(true);

    /**
     * Get a serializing transcoder with the default max data size.
     */
    public SerializingTranscoder() {
        this(CachedData.MAX_SIZE, true);
    }

    /**
     * Get a serializing transcoder that specifies the max data size.
     */
    public SerializingTranscoder(int max, boolean isMemcached) {
        super(max, isMemcached);
        if (isMemcached) {
            COMPRESSED = (1 << 4) | (1 << 5);
        } else {
            COMPRESSED = (1 << 1);
        }
    }

    @Override
    public boolean asyncDecode(CachedData d) {
        if ((d.getFlags() & COMPRESSED) != 0) {
            return true;
        }
        return super.asyncDecode(d);
    }

    /*
     * (non-Javadoc)
     *
     * @see net.spy.memcached.Transcoder#decode(net.spy.memcached.CachedData)
     */
    public Object decode(CachedData d) {
        byte[] data = d.getData();
        Object rv = null;
        if ((d.getFlags() & COMPRESSED) != 0) {
            data = decompress(d.getData());
        }

        if (data != null) {
            int flags = d.getFlags() & SPECIAL_MASK;
            if (flags != 0) {
                if (flags == SPECIAL_BYTEARRAY) {
                    rv = data;
                } else {
                    getLogger().warn("Undecodeable with flags %x", flags);
                }
            } else {
                rv = decodeString(data);
            }
        }

        return rv;
    }

    /*
     * (non-Javadoc)
     *
     * @see net.spy.memcached.Transcoder#encode(java.lang.Object)
     */
    public CachedData encode(Object o) {
        byte[] b = null;
        int flags = 0;

        if (o instanceof String) {
            b = encodeString((String) o);
        } else if (o instanceof Number) {
            b = encodeString(o.toString());
        } else if (o instanceof byte[]) {
            b = (byte[]) o;
            flags |= SPECIAL_BYTEARRAY;
        } else {
            throw new NullPointerException("Not support object type " + o.getClass().getCanonicalName());
        }

        if (b.length > compressionThreshold) {
            byte[] compressed = compress(b);
            if (compressed.length < b.length) {
                b = compressed;
                flags |= COMPRESSED;
            }
        }
        return new CachedData(flags, b, getMaxSize());
    }
}
