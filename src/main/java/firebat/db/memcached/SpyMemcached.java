package firebat.db.memcached;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactory;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;
import net.spy.memcached.CASResponse;
import net.spy.memcached.internal.OperationFuture;

import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class SpyMemcached extends AbstractDbKeyValue
{
    private final String id;
    private final String bucketName, bucketIP;
    private final CouchbaseClient client;
    private final SerializingTranscoder decoder;    
    
    public SpyMemcached (String id , String bucketName, String bucketIP, int operationTimeOut, int compressThreshold, boolean isMemcached, int maxKeySize) throws Exception
    {
        this.id = id;
        this.bucketName = bucketName;
        this.bucketIP = bucketIP;
        ArrayList<URI> baseURIs = new ArrayList<>();
        baseURIs.add(new URI("http://" + bucketIP + ":8091/pools"));

        CouchbaseConnectionFactoryBuilder cfb = new CouchbaseConnectionFactoryBuilder();
        if (operationTimeOut > 0)
            cfb.setOpTimeout(operationTimeOut);
        CouchbaseConnectionFactory cf = cfb.buildCouchbaseConnection(baseURIs, bucketName, "");
        decoder = new SerializingTranscoder(maxKeySize, isMemcached);
        if (compressThreshold > 0)
            decoder.setCompressionThreshold(compressThreshold);

        client = new CouchbaseClient(cf);
    }

    @Override
    public void disconnect ()
    {
        client.shutdown();
    }

    @Override
    public String getId ()
    {
        return id;
    }

    @Override
    public boolean set (String key, Object value)
    {
        return set(key, value, AbstractDbKeyValue.NO_EXPIRATION);
    }
    
    @Override
    public boolean set (String key, Object value, int expiration)
    {
        OperationFuture<Boolean> f = client.set(key, modifyExpiration(expiration), value, decoder);
        try
        {
            return f.get().booleanValue();
        } 
        catch (Exception e) 
        {
            f.cancel();
        }   
        return false;
    }

    @Override
    public boolean asyncSet(String key, Object value) {
        client.set(key, AbstractDbKeyValue.NO_EXPIRATION, value, decoder);
        return true;
    }

    @Override
    public boolean asyncSet(String key, Object value, int expiration) {
        client.set(key, modifyExpiration(expiration), value, decoder);
        return true;
    }

    @Override
    public Object get (String key)
    {
        return client.get(key, decoder);
    }
    
    @Override
    public CASValue gets (String key)
    {
        net.spy.memcached.CASValue v = client.gets(key, decoder);
        if (v == null)
        {
            return null;
        }
        return new CASValue(v.getCas(), v.getValue());
    }
    
    @Override
    public boolean cas (String key, long cas, Object value)
    {
        return client.cas(key, cas, value, decoder) == CASResponse.OK;
    }

    @Override
    public Map<String, Object> getMulti (Collection<String> keys)
    {
        return client.getBulk(keys, decoder);
    }

    @Override
    public boolean delete (String key)
    {
        OperationFuture<Boolean> f = client.delete(key);
        try
        {
            return f.get().booleanValue();
        } 
        catch (Exception e) 
        {
            f.cancel();
        }   
        return false;
    }

    @Override
    public boolean add (String key, Object value)
    {
        return add(key, value, AbstractDbKeyValue.NO_EXPIRATION);
    }
    
    @Override
    public boolean add (String key, Object value, int expiration)
    {
        OperationFuture<Boolean> f = client.add(key, modifyExpiration(expiration), value, decoder);
        try
        {
            return f.get().booleanValue();
        } 
        catch (Exception e) 
        {
            f.cancel();
        }   
        return false;
    }

    @Override
    public boolean replace (String key, Object value)
    {
        return replace(key, value, AbstractDbKeyValue.NO_EXPIRATION);
    }
    
    @Override
    public boolean replace (String key, Object value, int expiration)
    {
        OperationFuture<Boolean> f = client.replace(key, modifyExpiration(expiration), value, decoder);
        try
        {
            return f.get().booleanValue();
        } 
        catch (Exception e) 
        {
            f.cancel();
        }   
        return false;
    }

    @Override
    public void asyncDecr (String key, long offset)
    {
        client.asyncDecr(key, offset);
    }

    @Override
    public void asyncIncr (String key, long offset)
    {
        client.asyncIncr(key, offset);
    }

    @Override
    public long decrement (String key, long offset)
    {
        return client.decr(key, offset);
    }

    @Override
    public long decrement (String key, long offset, long initialValue)
    {
        return client.decr(key, offset, initialValue);
    }

    @Override
    public long decrement (String key, long offset, long initialValue, int expiration)
    {
        return client.decr(key, offset, initialValue, modifyExpiration(expiration));
    }

    @Override
    public long increment (String key, long offset)
    {
        return client.incr(key, offset);
    }

    @Override
    public long increment (String key, long offset, long initialValue)
    {
        return client.incr(key, offset, initialValue);
    }

    @Override
    public long increment (String key, long offset, long initialValue, int expiration)
    {
        return client.incr(key, offset, initialValue, modifyExpiration(expiration));
    }

    @Override
    public boolean append (String key, String value, long cas)
    {
        OperationFuture<Boolean> f = client.append(cas , key , value, decoder);
        try
        {
            return f.get().booleanValue();
        } 
        catch (Exception e) 
        {
            f.cancel();
        }   
        return false;
    }

    @Override
    public boolean touch (String key, int expiration)
    {
        OperationFuture<Boolean> f = client.touch(key, modifyExpiration(expiration), decoder);
        try
        {
            return f.get().booleanValue();
        }
        catch (Exception e)
        {
            f.cancel();
        }
        return false;
    }

    @Override
    public Map<SocketAddress, Map<String, String>> getStats ()
    {
        return client.getStats();
    }
}
