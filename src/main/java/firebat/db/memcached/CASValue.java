package firebat.db.memcached;

public class CASValue
{
    private final long cas;
    private final Object value;
    
    public CASValue (long cas, Object value)
    {
        this.cas = cas;
        this.value = value;
    }

    public long getCas ()
    {
        return cas;
    }

    public Object getValue ()
    {
        return value;
    }
}
